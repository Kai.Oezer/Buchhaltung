// swift-tools-version:6.0

import PackageDescription

let package = Package(
	name: "Buchhaltung",
	platforms: [.iOS(.v17), .macOS(.v14), .tvOS(.v17), .watchOS(.v10)],
	products: [
		.library( name: "Buchhaltung", targets: ["Buchhaltung"]),
		.executable(name: "buchhalter", targets: ["BuchhaltungCLI"])
	],
	dependencies: [
		.package(url: "https://github.com/apple/swift-collections", from:"1.1.0"),
		.package(url: "https://github.com/apple/swift-argument-parser", from: "1.5.0"),
		.package(url: "https://gitlab.com/belanz/DEGAAP", from: "0.14.0"),
		.package(url: "https://gitlab.com/Kai.Oezer/TOMLike", from: "0.6.0"),
		.package(url: "https://gitlab.com/Kai.Oezer/IssueCollection", from: "0.5.2"),
		.package(url: "https://github.com/apple/swift-log", from: "1.6.1")
	],
	targets: [
		_libraryTarget,
		_commandLineToolTarget,
		_testTarget
	]
)

private var _libraryTarget : Target
{
	.target(
		name: "Buchhaltung",
		dependencies: [
			"DEGAAP",
			"TOMLike",
			"IssueCollection",
			.product(name: "OrderedCollections", package: "swift-collections"),
			.product(name: "DequeModule", package: "swift-collections"),
			.product(name: "Logging", package: "swift-log", condition: .when(platforms: [.linux, .android, .windows, .openbsd]))
		],
		exclude: [
			"Exporting/LaTeX/example"
		],
		resources: [
			.copy("Resources/LaTeX"),
			.copy("Resources/myebilanz_ini_csv.txt")
		]
	)
}

private var _commandLineToolTarget : Target
{
	.executableTarget(
		name: "BuchhaltungCLI",
		dependencies: [
			"Buchhaltung",
			.product(name: "ArgumentParser", package: "swift-argument-parser")
		]
	)
}

private var _testTarget : Target
{
	.testTarget(
		name: "BuchhaltungTests",
		dependencies: ["Buchhaltung"]
	)
}
