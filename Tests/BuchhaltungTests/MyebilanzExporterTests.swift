// Copyright 2022-2024 Kai Oezer

import Testing
@testable import Buchhaltung
import IssueCollection

struct MyebilanzExporterTests
{
	@Test
	func exporting() throws
	{
		var decodingIssues = IssueCollection()

		let report = try Report(fromArchive: TestData.archivedFiscalReport3, issues: &decodingIssues)
		let (ini, csv) = try MyebilanzExporter.exportWithCSV(report: report)
		#expect(ini == TestData.myebilanzINI1)
		#expect(csv == TestData.myebilanzCSV1)

		let report2 = try Report(fromArchive: TestData.archivedFiscalReport4, issues: &decodingIssues)
		let (ini2, csv2) = try MyebilanzExporter.exportWithCSV(report: report2)
		#expect(ini2 == TestData.myebilanzINI2)
		#expect(csv2 == TestData.myebilanzCSV2)

		let report3 = try Report(fromArchive: TestData.archivedFiscalReport5, issues: &decodingIssues)
		let (ini3, csv3) = try MyebilanzExporter.exportWithCSV(report: report3)
		#expect(ini3 == TestData.myebilanzINI3)
		#expect(csv3 == TestData.myebilanzCSV3)
		let ini3WithAccountValues = try MyebilanzExporter.export(report: report3)
		#expect(ini3WithAccountValues == TestData.myebilanzINI3WithAccountValues)

		#expect(decodingIssues.count == 0)
	}

	@Test("replacing German characters")
	func replacingGermanCharacters() throws
	{
		var company = Company()
		company.name = ["Ulkig UG (haftungsbeschränkt)"]
		company.seat = Company.Seat(street: "Münchner Ring",
			cityOrRegion: "Unterschleißheim",
			country: "Ort mit Ö und ö und Ä und ä und Ü und ü und nochmal Ö ä Ü")
		let report = Report(
			title: ReportTitle("Report"),
			type: .trade,
			taxonomy: .v6_5,
			period: .year(2023)!,
			inventory: Inventory(period: .year(2023)!),
			incomeStatement: IncomeStatement(),
			balanceSheet: BalanceSheet(),
			notesBelowBalanceSheet: NotesBelowBalanceSheet(companyInformation: company),
			notes: Notes(),
			accountTotals: AccountTotals(totals: [:])
		)
		let myebilanzINI = try MyebilanzExporter.export(report: report)

		#expect(myebilanzINI.firstMatch(of: #/[ÄäÖöÜüß]/#) == nil)
		#expect(myebilanzINI.range(of: "Ulkig UG (haftungsbeschraenkt)") != nil)
		#expect(myebilanzINI.range(of: "Unterschleissheim") != nil)
		#expect(myebilanzINI.range(of: "Ort mit Oe und oe und Ae und ae und Ue und ue und nochmal Oe ae Ue") != nil)
	}

}
