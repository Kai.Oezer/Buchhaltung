// Copyright 2022-2025 Kai Oezer

import Testing
import Foundation
@testable import Buchhaltung

struct DayTests
{
	@Test
	func initialization()
	{
		#expect(Day(2025,1,31) != nil)
		#expect(Day(-100,1,40) == nil)
		#expect(Day(2025,1,40) == nil)
		#expect(Day(2025,14,31) == nil)
		#expect(Day(internetDate: "2025-01-31T12:00:00+01:00") != nil)
		#expect(Day(internetDate: "2025-01-40T12:00:00+01:00") == nil)
		#expect(Day(internetDate: "2025-01-31T12:00:00") == nil)
		#expect(Day(string: "2025-02-01") != nil)
		#expect(Day(string: "2025-22-33") == nil)
		#expect(Day(2200,1,1) == Day.distantFuture)
		#expect(Day(3000,1,1) == Day.distantFuture)
		#expect(Day(2003,12,30) == Day.distantPast)
		#expect(Day(1990,1,1) == Day.distantPast)
	}

	@Test
	func validation()
	{
		#expect(Day(2024,12,1)!.isValid)
		#expect(Day.distantPast.isValid == false)
		#expect(Day.distantFuture.isValid == false)
		#expect(Day(2500,12,1)!.isValid == false)
		#expect(Day(1924,12,1)!.isValid == false)
	}

	@Test
	func comparing()
	{
		#expect(Day(2010, 3, 4)! > Day(2008, 6, 8)!)
		#expect(Day(2010, 3, 4)! < Day(2010, 6, 8)!)
		#expect(Day(2010, 3, 4)! > Day(2010, 3, 3)!)
		#expect(Day(2010, 3, 4)! < Day(2010, 3, 5)!)
		#expect(Day(2010, 3, 4)! <= Day(2010, 3, 4)!)
		#expect(Day(2010, 3, 4)! <= Day(2010, 3, 5)!)
		#expect((Day(2010, 3, 4)! <= Day(2010, 3, 3)!) == false)
		#expect(Day(2010, 3, 4)! >= Day(2008, 6, 8)!)
		#expect(Day(2010, 3, 4)! >= Day(2010, 3, 4)!)
		#expect((Day(2009, 3, 4)! >= Day(2010, 3, 4)!) == false)
	}

	@Test
	func comparingWithDate()
	{
		let date1 = Date(timeIntervalSince1970: 1_750_000_000) // 2025-06-15 17:06
		#expect(Day(2025,06,16)! >= date1)
		#expect(Day(2025,06,15)! >= date1)
		#expect((Day(2025,06,14)! >= date1) == false)
		#expect(Day(2025,06,14)! <= date1)
		#expect(Day(2025,06,15)! <= date1)
		#expect((Day(2025,06,16)! <= date1) == false)
	}

	@Test
	func dayOfMonth()
	{
		#expect(Day(2014, 5, 16)!.dayOfMonth == 16)
		#expect(Day(2014, 10, 23)!.dayOfMonth == 23)
	}

	@Test
	func isEndOfMonth()
	{
		#expect(!Day(2012, 3, 10)!.isEndOfMonth)
		#expect(Day(2012, 3, 31)!.isEndOfMonth)
	}

	@Test("adding and subtracting")
	func addingAndSubtracting()
	{
		#expect(Day(2018, 4, 5)! + 10 == Day(2018, 4, 15))
		#expect(Day(2018, 4, 5)! - 10 == Day(2018, 3, 26))
	}

	@Test
	func nextYear()
	{
		#expect(Day(2023,6,1)!.nextYear == Day(2024,6,1)!)
		#expect(Day.distantPast.nextYear == Day.distantPast)
		#expect(Day.distantFuture.nextYear == Day.distantFuture)
	}

	@Test
	func interval() throws
	{
		let day = try #require(Day(2025,2,1))
		#expect(day.interval.start == day.start)
		#expect(day.interval.duration.rounded() == Double(60 * 60 * 24))
	}

	@Test("year, month, dateComponents")
	func dateComponents() throws
	{
		let day = try #require(Day(2025, 2, 1))
		#expect(day.year == 2025)
		#expect(day.month == 2)
		let components = day.dateComponents
		#expect(components.year == 2025)
		#expect(components.month == 2)
		#expect(components.day == 1)
		#expect(components.hour == nil)
		#expect(components.minute == nil)
		#expect(components.second == nil)
	}

	@Test
	func containsDate() throws
	{
		let day = try #require(Day(2025, 4, 18))
		#expect(day.contains(Date(timeIntervalSince1970: 1_745_000_000))) // 2025-04-18 20:13
		#expect(day.contains(Date(timeIntervalSince1970: 1_740_000_000)) == false) // 2025-02-19 22:20
	}

	@Test
	func description()
	{
		#expect(Day(2024, 4, 16)!.description == "2024-04-16")
	}

	@Test("BH formatting", .tags(.bhCoding))
	func bhFormatting() throws
	{
		let formattedDay = Day(2025, 4, 1)!.formatted(.bhDay)
		#expect(formattedDay == "2025-04-01")
	}

	@Test("BH parsing", .tags(.bhCoding))
	func bhParsing() throws
	{
		let day = try Day("2026-06-20", strategy: .bhDay)
		#expect(day == Day(2026,6,20)!)
		#expect(throws: BHCodingError.self) {
			try Day.BHFormatStyle().parseStrategy.parse("Blabla")
		}
	}

#if false
	@Test
	func sortingPerformance()
	{
		let numberOfItems = 10_000
		let items = Array<Day>(unsafeUninitializedCapacity: numberOfItems) { buffer, numInitialized in
			if let baseAddress = buffer.baseAddress {
				for index in 1...numberOfItems {
					baseAddress.advanced(by: index).pointee = Day(
						Int.random(in: 2010...2180), Int.random(in: 1...12), Int.random(in: 1...28)
					)!
				}
				numInitialized = numberOfItems
			}
		}
		measure(metrics: [XCTClockMetric()]) {
			_ = items.sorted()
		}
	}
#endif
}
