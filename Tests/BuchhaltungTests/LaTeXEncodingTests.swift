//  Copyright 2020-2024 Kai Oezer

import Foundation
import Testing
@testable import Buchhaltung
import TOMLike
import IssueCollection

struct LaTeXEncodingTests
{
	@Test("template loading")
	func templateLoading() throws
	{
		let closing = try LaTeXEncoder.contents(ofTemplate: "closing")
		#expect(closing == "\\end{document}\n")
	}

	@Test("string replacement")
	func stringReplacement() throws
	{
		var template =
			"""
			\\BHPlaceholder{Hello} mello yello abracadabra
			live music transmission \\BHPlaceholder{Hello} in the
			receiver selection safe document number \\BHPlaceholder{Hello}.
			"""
		let expected =
			"""
			World mello yello abracadabra
			live music transmission World in the
			receiver selection safe document number World.
			"""
		try LaTeXEncoder.replace(placeholder: "Hello", in: &template, with: "World")
		#expect(template == expected)

		#expect(throws: LaTeXEncodingError.placeholderNotFound) {
			try LaTeXEncoder.replace(placeholder: "Larry", in: &template, with: "Harry")
		}
	}

	@Test("generating inventory fixed assets listing")
	func generatingInventoryFixedAssetsListing() throws
	{
		var decodingIssues = IssueCollection()
		let assets = try TOMLikeCoder.decode(Assets.self, from: _assetsArchive1, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(assets.fixedAssets.count == 2)
		let latexAssetListing = Inventory(period: Period.year(2020)!, assets: assets).fixedAssetsListing
		#expect(latexAssetListing == _expectedFixedAssetsRepresentation1)
	}

	@Test("generating inventory overview")
	func generatingInventoryOverview() throws
	{
		var decodingIssues = IssueCollection()
		let assets = try TOMLikeCoder.decode(Assets.self, from: _assetsArchive2, issues: &decodingIssues)
		let debts = try TOMLikeCoder.decode(Debts.self, from: _debtsArchive1, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		let statement = Inventory(period: Period.year(2021)!, assets: assets, debts: debts)
		let overview = statement.overview
		#expect(overview == _expectedOverview)
		let assetList = statement.fixedAssetsListing
		#expect(assetList == _expectedAssetList)
	}

	let _assetsArchive1 =
		"""
		= 1
		equipment
		"computer display"
		acqd 2020-04-13
		cost 249,00
		depr linear 5
		= 1a
		equipment
		"HDMI cable"
		acqd 2020-06-09
		cost 20,00
		depr linear 7
		"""

	let _expectedFixedAssetsRepresentation1 =
		"""
		\\hline
		\\multicolumn{1}{|r|}{1} & \\multicolumn{2}{l|}{computer display} \\\\
		\\hline
			& 13.04.2020 & 249,00 \\euro \\\\
			& linear auf 5 Jahre & 211,65 \\euro \\\\
		\\hline
		\\multicolumn{1}{|r|}{1a} & \\multicolumn{2}{l|}{HDMI cable} \\\\
		\\hline
			& 09.06.2020 & 20,00 \\euro \\\\
			& linear auf 7 Jahre & 18,39 \\euro \\\\
		"""

	let _assetsArchive2 =
		"""
		= 1
		equipment
		"computer display"
		acqd 2020-04-13
		cost 249,00
		depr linear 5
		= 5
		building
		"building Cortex Street 1"
		acqd 2021-03-09
		cost 80000,00
		"""

	let _debtsArchive1 =
		"""
		-
		"mortgage"
		loan 2021-03-01 60000,00
		paym 2021-09-01 10000,00
		paym 2022-03-01 10000,00
		paym 2022-09-01 10000,00
		paym 2023-03-01 10000,00
		paym 2023-09-01 10000,00
		paym 2024-03-01 10000,00
		-
		"laptop"
		loan 2021-05-05 2500
		paym 2022-02-04 2500
		"""

	let _expectedOverview =
		"""
		\\multicolumn{4}{l|}{\\textbf{A. Vermögen}} & \\\\
		& \\multicolumn{3}{l|}{\\textbf{I. Anlagevermögen}} & 161,85 \\euro \\\\
		&  & Sonstige Betriebs- und Geschäftsausstattung & 161,85 \\euro & \\\\
		& \\multicolumn{3}{l|}{\\textbf{II. Umlaufvermögen}} & 0,00 \\euro \\\\

		\\hline
		\\multicolumn{4}{|l|}{\\textbf{Summe Vermögen}} & \\multicolumn{1}{r|}{161,85 \\euro} \\\\
		\\hline
		\\multicolumn{4}{l|}{} & \\\\
		\\multicolumn{4}{l|}{\\textbf{B. Schulden}} & \\\\
		& \\multicolumn{3}{l|}{\\textbf{I. Langfristige Schulden}} & 50.000,00 \\euro \\\\
		& \\multicolumn{3}{l|}{\\textbf{II. Kurzfristige Schulden}} & 2.500,00 \\euro \\\\
		\\hline
		\\multicolumn{4}{|l|}{\\textbf{Summe Schulden}} & \\multicolumn{1}{r|}{52.500,00 \\euro} \\\\
		\\hline
		\\multicolumn{4}{l|}{} & \\\\
		\\multicolumn{4}{l|}{\\textbf{C. Errechnen des Reinvermögens}} & \\\\
		& \\multicolumn{3}{l|}{$+$ Summe des Vermögens} & 161,85 \\euro \\\\
		& \\multicolumn{3}{l|}{$-$ Summe der Schulden} & 52.500,00 \\euro \\\\
		\\hline
		\\multicolumn{4}{|l|}{\\textbf{Reinvermögen (Eigenkapital)}} & \\multicolumn{1}{r|}{-52.338,15 \\euro} \\\\
		\\hline
		"""

	let _expectedAssetList =
		"""
		\\hline
		\\multicolumn{1}{|r|}{1} & \\multicolumn{2}{l|}{computer display} \\\\
		\\hline
			& 13.04.2020 & 249,00 \\euro \\\\
			& linear auf 5 Jahre & 161,85 \\euro \\\\
		"""
}
