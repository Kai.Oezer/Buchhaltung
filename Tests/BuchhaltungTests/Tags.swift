// Copyright 2025 Kai Oezer

import Testing

extension Tag
{
	@Tag static var bhCoding : Self
	@Tag static var tomlike : Self
	@Tag static var recordGrouping : Self
}
