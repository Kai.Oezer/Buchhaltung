// Copyright 2021-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import TOMLike
import IssueCollection

struct BalanceSheetTests
{
	@Test("BH encoding", .tags(.bhCoding))
	func bhEncoding() throws
	{
		let archivedBalanceSheet = try #require(try? TOMLikeCoder.encode(TestData.balanceSheet1))
		#expect(archivedBalanceSheet == TestData.archivedBalanceSheet1)
		let archiveBalanceSheet2 = try #require(try? TOMLikeCoder.encode(TestData.balanceSheet2))
		#expect(archiveBalanceSheet2 == TestData.archivedBalanceSheet2)
	}

	@Test("BH decoding", .tags(.bhCoding))
	func bhDecoding() throws
	{
		var decodingIssues = IssueCollection()
		let balanceSheet = try #require(try? TOMLikeCoder.decode(BalanceSheet.self, from: TestData.archivedBalanceSheet1, issues: &decodingIssues))
		#expect(balanceSheet == TestData.balanceSheet1)
		let balanceSheet2 = try #require(try? TOMLikeCoder.decode(BalanceSheet.self, from: TestData.archivedBalanceSheet2, issues: &decodingIssues))
		#expect(balanceSheet2 == TestData.balanceSheet2)
		#expect(decodingIssues.count == 0)
	}
}
