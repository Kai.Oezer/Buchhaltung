// Copyright 2020-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import DEGAAP
import IssueCollection
import TOMLike

struct LedgerTests
{
	private let _degaap = DEGAAPEnvironment()

	@Test("generating an empty report")
	func emptyReportGeneration() async
	{
		let testJournal = Journal()
		var testCompany = Company()
		testCompany.name = ["Test"]
		testCompany.seat = Company.Seat()
		testCompany.subscribedCapital = 1000
		let testAssets = Assets()
		let bookkeeping = Ledger(period: Period.currentYear!, company: testCompany, journal: testJournal, assets: testAssets)
		var issues = IssueCollection()
		let report = try? await bookkeeping.generateReport(type: .trade, using: _degaap, issues: &issues)
		#expect(issues.count == 0)
		#expect(report != nil)
	}

	@Test
	func archiving() throws
	{
		let archive = try TOMLikeCoder.encode(TestData.ledger1)
		#expect(archive == TestData.archivedLedger1)
	}

	@Test
	func unarchiving() throws
	{
		var decodingIssues = IssueCollection()
		let ledger = try Ledger(fromArchive: TestData.archivedLedger3, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(ledger.assets.fixedAssets.count == 1)
		#expect(ledger.journal.records.count == 7)
		#expect(ledger.journal.records[3].debit.count == 2)
		#expect(ledger.journal.records[4].debit.count == 1)
		#expect(ledger.journal.records[4].debit.first!.value == 19.90)
	}

	@Test("unarchiving with issues")
	func unarchivingWithIssues() async throws
	{
		var issues = IssueCollection()
		let ledger = try Ledger(fromArchive: TestData.archivedLedgerWithIssues1, issues: &issues)
		#expect(issues.count == 5)
		await ledger.checkForIssues(using: _degaap, issues: &issues)
		#expect(ledger.journal.records.count == 16)
		#expect(issues.issues(for: .bhCoding).filter{$0.code == .duplicateRecord}.count == 1) // duplicate record "Miete"
		#expect(issues.issues(for: .bhCoding).filter{$0.code == .moneyFormat}.count == 1) // money with decimal separator '.' instead of ','
		#expect(issues.issues(for: .bhCoding).filter{$0.code == .gwgButNotEquipment}.count == 1) // 'Fahrrad' classified as 'machinery' instead of 'equipment'
		#expect(issues.issues(for: .bhCoding).filter{$0.code == .derivedReceivableAsset}.count == 2) // current assets with description 'derived', which is a reserved string
		#expect(issues.issues(for: .ledger).count == 2)
		#expect(issues.issues(for: .ledger).filter{$0.code == .journalFixedAssetNotListed}.count == 1)
		#expect(issues.issues(for: .ledger).filter{$0.code == .retiredFixedAssetsNotAppreciatedInJournal}.count == 1)
	}
	
	@Test("unarchiving and generating reports")
	func unarchivingAndGeneratingReports() async throws
	{
		var issues = IssueCollection()
		
		let ledger1 = try Ledger(fromArchive: TestData.archivedLedger3, issues: &issues)
		await ledger1.checkForIssues(using: _degaap, issues: &issues)
		#expect(issues.count == 0)
		
		let tradeReport = try await ledger1.generateReport(type:.trade, withInventory: true, using: _degaap, issues: &issues)
		#expect(tradeReport.balanceSheet.isBalanced)
		#expect(tradeReport.incomeStatement.results.profitLoss == -318.67)
		#expect(tradeReport.balanceSheet.assets.fixedAssets == 102.38)
		#expect(tradeReport.balanceSheet.assets.sum == 6831.33)
		#expect(tradeReport.inventory.total == tradeReport.balanceSheet.eqLiab.equity)
		#expect(tradeReport.notes.assetChangesStatement == nil)
			
		let taxReport = try await ledger1.generateReport(type:.fiscal, withInventory: true, using: _degaap, issues: &issues)
		#expect(taxReport.notes.assetChangesStatement?.changes.count == 1)
		#expect(taxReport.notes.assetChangesStatement?.changes.first?.value.acquisitionCosts.end == 108.40)
		
		#expect(issues.count == 0)
	}

	@Test("generating successive fiscal reports")
	func generatingSuccessiveFiscalReports() async throws
	{
		var issues = IssueCollection()
		
		let ledger1 = try Ledger(fromArchive: TestData.archivedLedger3, issues: &issues)
		let fiscalReport1 = try await ledger1.generateReport(type:.fiscal, withInventory: true, using: _degaap, issues: &issues)
		let archivedReport1 = try TOMLikeCoder.encode(fiscalReport1)
		#expect(archivedReport1 == TestData.archivedFiscalReport3)
		#expect(fiscalReport1.balanceSheet.assets.sum == 6831.33)
		#expect(fiscalReport1.accountTotals.totals[DEGAAPAccounts.eqLiab_equity_subscribed] == -7000)
		#expect(fiscalReport1.balanceSheet.isBalanced)
		#expect(fiscalReport1.notes.assetChangesStatement?.changes.count == 1)
		#expect(fiscalReport1.notes.assetChangesStatement?.changes.first?.value.bookValues.current == 102.38)
		
		var ledger2 = try Ledger(fromArchive: TestData.archivedLedger4, issues: &issues)
		#expect(ledger2.assets.fixedAssets.count == 2)
		#expect(ledger2.journal.records.count == 16)
		ledger2.baseReport = fiscalReport1
		
		let fiscalReport2 = try await ledger2.generateReport(type:.fiscal, using: _degaap, issues: &issues)
		#expect(fiscalReport2.balanceSheet.isBalanced)
		#expect(fiscalReport2.incomeStatement.results.profitLoss == -892.51)
		#expect(fiscalReport2.balanceSheet.assets.sum == 5788.82)
		#expect(fiscalReport2.inventory.total == fiscalReport2.balanceSheet.eqLiab.equity)
		#expect(fiscalReport2.notes.assetChangesStatement?.changes.count == 2)
		#expect(fiscalReport2.notes.assetChangesStatement?.total.bookValues.current == 67.26)
		
		var ledger3 = try Ledger(fromArchive: TestData.archivedLedger5, issues: &issues)
		#expect(ledger3.assets.fixedAssets.count == 4)
		#expect(ledger3.assets.currentAssets.count == 1)
		#expect(ledger3.journal.records.count == 16)
		ledger3.baseReport = fiscalReport2
		
		let fiscalReport3 = try await ledger3.generateReport(type: .fiscal, using: _degaap, issues: &issues)
		#expect(fiscalReport3.incomeStatement.results.profitLoss == -2137.95)
		#expect(fiscalReport3.incomeStatement.results.depreciations == 1511.44)
		#expect(fiscalReport3.balanceSheet.assets.sum == 3650.87)
		#expect(fiscalReport3.balanceSheet.isBalanced)
	}

	@Test("generating successive trade reports")
	func generatingSuccessiveTradeReports() async throws
	{
		var issues = IssueCollection()
		
		let ledger1 = try Ledger(fromArchive: TestData.archivedLedger3, issues: &issues)
		let tradeReport1 = try await ledger1.generateReport(type:.trade, withInventory: true, using: _degaap, issues: &issues)
		#expect(tradeReport1.balanceSheet.isBalanced)
		#expect(tradeReport1.balanceSheet.assets.sum == 6831.33)

		var ledger2 = try Ledger(fromArchive: TestData.archivedLedger4, issues: &issues)
		ledger2.baseReport = tradeReport1
		let tradeReport2 = try await ledger2.generateReport(type:.trade, using: _degaap, issues: &issues)
		#expect(tradeReport2.balanceSheet.isBalanced)
		#expect(tradeReport2.incomeStatement.results.profitLoss == -892.51)
		#expect(tradeReport2.balanceSheet.assets.sum == 5788.82)
		#expect(tradeReport2.inventory.total == tradeReport2.balanceSheet.eqLiab.equity)

		var ledger3 = try Ledger(fromArchive: TestData.archivedLedger5, issues: &issues)
		ledger3.baseReport = tradeReport2
		
		let tradeReport3 = try await ledger3.generateReport(type: .trade, using: _degaap, issues: &issues)
		#expect(tradeReport3.incomeStatement.results.profitLoss == -2137.95)
		#expect(tradeReport3.balanceSheet.assets.sum == 3650.87)
		#expect(tradeReport3.balanceSheet.isBalanced)
		#expect(issues.count == 0)
	}

	@Test("generating successive fiscal reports with intermediate archiving")
	func generatingSuccessiveFiscalReportsWithIntermediateArchiving() async throws
	{
		var issues = IssueCollection()
		let ledger1 = try #require(Ledger(fromArchive: TestData.archivedLedger3, issues: &issues))
		let fiscalReport1 = try #require(await ledger1.generateReport(type:.fiscal, withInventory: true, using: _degaap, issues: &issues))
		let encodedFiscalReport1 = try #require(try TOMLikeCoder.encode(fiscalReport1))
		let decodedFiscalReport1 = try #require(TOMLikeCoder.decode(Report.self, from: encodedFiscalReport1, issues: &issues))
		var ledger2 = try #require(Ledger(fromArchive: TestData.archivedLedger4, issues: &issues))
		ledger2.baseReport = decodedFiscalReport1
		let fiscalReport2 = try #require(await ledger2.generateReport(type:.fiscal, using: _degaap, issues: &issues))
		#expect(fiscalReport2.balanceSheet.isBalanced)
		#expect(fiscalReport2.incomeStatement.results.profitLoss == -892.51)
		#expect(fiscalReport2.balanceSheet.assets.sum == 5788.82)
		#expect(fiscalReport2.inventory.total == fiscalReport2.balanceSheet.eqLiab.equity)
		#expect(fiscalReport2.notes.assetChangesStatement?.changes.count == 2)
		#expect(fiscalReport2.notes.assetChangesStatement?.total.bookValues.current == 67.26)
		#expect(issues.count == 0)
	}
	
	@Test("generating successive trade reports with intermediate archiving")
	func generatingSuccessiveTradeReportsWithIntermediateArchiving() async throws
	{
		var issues = IssueCollection()
		let ledger1 = try #require(Ledger(fromArchive: TestData.archivedLedger3, issues: &issues))
		let tradeReport1 = try #require(await ledger1.generateReport(type:.trade, withInventory: true, using: _degaap, issues: &issues))
		#expect(tradeReport1.notes.assetChangesStatement == nil)
		let encodedTradeReport1 = try #require(try TOMLikeCoder.encode(tradeReport1))
		let decodedTradeReport1 = try #require(TOMLikeCoder.decode(Report.self, from: encodedTradeReport1, issues: &issues))
		var ledger2 = try #require(Ledger(fromArchive: TestData.archivedLedger4, issues: &issues))
		ledger2.baseReport = decodedTradeReport1
		let tradeReport2 = try #require(await ledger2.generateReport(type:.trade, using: _degaap, issues: &issues))
		#expect(tradeReport2.balanceSheet.assets.sum == 5788.82)
		#expect(tradeReport2.incomeStatement.results.profitLoss == -892.51)
		#expect(tradeReport2.inventory.total == tradeReport2.balanceSheet.eqLiab.equity)
		#expect(tradeReport2.balanceSheet.isBalanced)
		#expect(tradeReport2.notes.assetChangesStatement == nil)
		#expect(issues.count == 0)
	}

	@Test("LaTeX report generation")
	func latexReportGeneration() async throws
	{
		var issues = IssueCollection()
		let ledger1 = try Ledger(fromArchive: TestData.archivedLedger3, issues: &issues)
		let fiscalReport1 = try await ledger1.generateReport(type:.fiscal, using: _degaap, issues: &issues)
		let latexReport1 = try LaTeXEncoder.encode(fiscalReport1)
		#expect(issues.count == 0)
		#expect(!latexReport1.isEmpty)
		#expect(latexReport1.contains("{\\large\\textbf{Bilanz}}"))
		#expect(latexReport1.contains("AV  & 102,38 \\euro & \\vjhr{0,00 \\euro}"))
		#expect(latexReport1.contains("\\textbf{Anlagenspiegel}"))
		#expect(!latexReport1.contains("\\textbf{Inventar}"))
	}

	@Test("LaTeX report generation with inventory")
	func latexReportGenerationWithInventory() async throws
	{
		var issues = IssueCollection()
		let ledger1 = try Ledger(fromArchive: TestData.archivedLedger3, issues: &issues)
		let fiscalReport1 = try await ledger1.generateReport(type:.fiscal, withInventory: true, using: _degaap, issues: &issues)
		#expect(issues.count == 0)
		let latexReport1WithInventory = try LaTeXEncoder.encode(fiscalReport1)
		#expect(latexReport1WithInventory.contains("\\textbf{Inventar}"))
	}

	@Test("BVV")
	func bvv() async throws
	{
		var issues = IssueCollection()
		var ledger = try Ledger(fromArchive: TestData.archivedLedger5, issues: &issues)
		await ledger.checkForIssues(using: _degaap, issues: &issues)
		#expect(issues.count == 0)
		let previousYearReport = try Report(fromArchive: TestData.archivedFiscalReport4, issues: &issues)
		#expect(issues.count == 0)
		ledger.baseReport = previousYearReport
		let generatedReport = try await ledger.generateReport(type: .fiscal, using: _degaap, issues: &issues)
		let unarchivedReport = try Report(fromArchive: TestData.archivedFiscalReport5, issues: &issues)
		#expect(issues.count == 0)
		#expect(unarchivedReport.bvv != nil)
		#expect(unarchivedReport.bvv?.totals[DEGAAPAccounts.bvv_assets_current] == -3650.87)
		#expect(generatedReport == unarchivedReport)
	}

	@Test
	func accountTotals() async throws
	{
		var issues = IssueCollection()
		let journal = try #require(TOMLikeCoder.decode(Journal.self, from: TestData.archivedJournal_Sonderabschreibung, issues: &issues))
		#expect(issues.count == 0)
		let ledger = Ledger(period: .year(2024)!, company: Company(), journal: journal)
		let resolvedJournal = await ledger.journalByResolvingAccountMapping(of: journal, using: _degaap, issues: &issues)
		let accountTotals = resolvedJournal.accountTotals.totals
		#expect(issues.count == 0)
		try #require(!accountTotals.isEmpty)
		#expect(accountTotals.count == 6)
		#expect(accountTotals[AccountID("is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.otherMisc")!]! == 3575)
		#expect(accountTotals[AccountID("is.netIncome.regular.operatingTC.otherCost.disposFixAss.bookValue.tan")!]! == -1)
	}
}
