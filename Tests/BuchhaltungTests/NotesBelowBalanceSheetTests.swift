//  Copyright 2021-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import TOMLike
import IssueCollection

struct NotesBelowBalanceSheetTests
{
	@Test("BH decoding", .tags(.bhCoding))
	func bhDecoding() throws
	{
		var decodingIssues = IssueCollection()
		let notes1 = try TOMLikeCoder.decode(NotesBelowBalanceSheet.self, from: TestData.archivedNotesBelowBalanceSheet1, issues: &decodingIssues)
		#expect(notes1.companyInformation.name == TestData.notesBelowBalanceSheet1.companyInformation.name)
		#expect(notes1.companyInformation.seat == TestData.notesBelowBalanceSheet1.companyInformation.seat)
		#expect(notes1.companyInformation.tradeRegistration.tradeCourt == TestData.notesBelowBalanceSheet1.companyInformation.tradeRegistration.tradeCourt)
		#expect(notes1.companyInformation.tradeRegistration.tradeRegistrationID == TestData.notesBelowBalanceSheet1.companyInformation.tradeRegistration.tradeRegistrationID)
		#expect(notes1.companyInformation.size == TestData.notesBelowBalanceSheet1.companyInformation.size)
		#expect(notes1.companyInformation.inLiquidation == false)
		#expect(notes1.incomeCalculationMethod == .gkv)

		let notes2 = try TOMLikeCoder.decode(NotesBelowBalanceSheet.self, from: TestData.archivedNotesBelowBalanceSheet2, issues: &decodingIssues)
		#expect(notes2.companyInformation.name == TestData.notesBelowBalanceSheet2.companyInformation.name)
		#expect(notes2.companyInformation.seat == TestData.notesBelowBalanceSheet2.companyInformation.seat)
		#expect(notes2.companyInformation.tradeRegistration.tradeCourt == TestData.notesBelowBalanceSheet2.companyInformation.tradeRegistration.tradeCourt)
		#expect(notes2.companyInformation.tradeRegistration.tradeRegistrationID == TestData.notesBelowBalanceSheet2.companyInformation.tradeRegistration.tradeRegistrationID)
		#expect(notes2.companyInformation.size == TestData.notesBelowBalanceSheet2.companyInformation.size)
		#expect(notes2.companyInformation.inLiquidation == true)
		#expect(notes2.incomeCalculationMethod == .ukv)

		#expect(decodingIssues.count == 0)
	}

	@Test("BH encoding", .tags(.bhCoding))
	func bhEncoding() throws
	{
		let archive1 = try TOMLikeCoder.encode(TestData.notesBelowBalanceSheet1)
		#expect(archive1 == TestData.archivedNotesBelowBalanceSheet1)

		let archive2 = try TOMLikeCoder.encode(TestData.notesBelowBalanceSheet2)
		#expect(archive2 == TestData.archivedNotesBelowBalanceSheet2)
	}
}
