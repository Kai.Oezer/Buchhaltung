// Copyright 2021-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import TOMLike
import IssueCollection

struct InventoryTests
{
	@Test("BH encoding", .tags(.bhCoding))
	func bhEncoding() throws
	{
		let encodedStatement = try #require(try TOMLikeCoder.encode(TestData.inventory1))
		#expect(encodedStatement == TestData.archivedInventory1)
	}

	@Test("BH decoding", .tags(.bhCoding))
	func bhDecoding() throws
	{
		var decodingIssues = IssueCollection()
		#expect(decodingIssues.count == 0)
		let decodedStatement = try #require(try TOMLikeCoder.decode(Inventory.self, from: TestData.archivedInventory1, issues: &decodingIssues))
		#expect(decodedStatement.fixedAssetsValuation.count == 1)
		#expect(decodedStatement.currentAssetsValuation.count == 3)
		#expect(decodedStatement.shortTermDebtsValuation == 500)
	}
}
