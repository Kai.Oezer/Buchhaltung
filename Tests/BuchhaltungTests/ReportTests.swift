// Copyright 2021-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import TOMLike
import IssueCollection

struct ReportTests
{
	@Test("BH encoding", .tags(.bhCoding))
	func bhEncoding() throws
	{
		let archivedReport = try #require(try TOMLikeCoder.encode(TestData.report1))
		#expect(archivedReport == TestData.archivedTradeReport1)
	}

	@Test("BH decoding", .tags(.bhCoding))
	func bhDecoding() throws
	{
		var issues = IssueCollection()
		let report = try #require(TOMLikeCoder.decode(Report.self, from: TestData.archivedTradeReport1, issues: &issues))
		#expect(issues.count == 0)
		#expect(report == TestData.report1)
	}

	@Test(.tags(.tomlike))
	func reportTypeCoding() throws
	{
		let trade = ReportType.trade
		let encodedReportType = try TOMLikeCoder.encode(trade)
		#expect(encodedReportType == "Handelsbilanz")

		let input = "Umwandlungsbilanz"
		var decodingIssues = IssueCollection()
		var decodedReportType = try TOMLikeCoder.decode(ReportType.self, from: input, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(decodedReportType == .ub)

		let input2 = """
			Umwandlungsbilanz
			bla bla
			bla
			"""
		decodedReportType = try TOMLikeCoder.decode(ReportType.self, from: input2, issues: &decodingIssues)
		#expect(decodingIssues.count == 2)
		#expect(decodedReportType == .ub)
	}

	@Test(.tags(.tomlike))
	func reportTitleCoding() throws
	{
		let title = ReportTitle("Jahresabschluss 2025")
		let encodedTitle = try TOMLikeCoder.encode(title)
		#expect(encodedTitle == "Jahresabschluss 2025")
		
		let input = "Zwischenabschluss 2025/I"
		var decodingIssues = IssueCollection()
		var decodedTitle = try TOMLikeCoder.decode(ReportTitle.self, from: input, issues: &decodingIssues)
		#expect(decodingIssues.count == 0)
		#expect(decodedTitle == ReportTitle("Zwischenabschluss 2025/I"))

		let input2 = """
			Zwischenabschluss 2025/II
			xxxx yyy
			bla bla bla
			bla
			"""
		decodedTitle = try TOMLikeCoder.decode(ReportTitle.self, from: input2, issues: &decodingIssues)
		#expect(decodingIssues.count == 3)
		#expect(decodedTitle == ReportTitle("Zwischenabschluss 2025/II"))
	}
}
