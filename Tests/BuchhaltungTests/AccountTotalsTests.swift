// Copyright 2022-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import TOMLike
import IssueCollection

struct AccountTotalsTests
{
	@Test("BH encoding", .tags(.bhCoding))
	func bhEncoding() throws
	{
		var decodingIssues = IssueCollection()
		let decodedTotals = try #require(try? TOMLikeCoder.decode(AccountTotals.self, from: TestData.archivedAccountTotals1, issues: &decodingIssues))
		#expect(decodingIssues.count == 0)
		#expect(decodedTotals == TestData.accountTotals1)
	}

	@Test("BH decoding", .tags(.bhCoding))
	func bhDecoding() throws
	{
		let archivedTotals = try #require(try? TOMLikeCoder.encode(TestData.accountTotals1))
		#expect(archivedTotals == TestData.archivedAccountTotals1)
	}

	@Test
	func debugDescription()
	{
		let totals = AccountTotals(totals: [
			AccountID("test.example.2")! : 50.0,
			AccountID("test.example.1")! : 10.0,
			AccountID("example.3")! : 100.0,
		])
		#expect(totals.debugDescription == """
			example.3 100,00
			test.example.1 10,00
			test.example.2 50,00
			"""
		)
	}

	@Test
	func addition()
	{
		let totals1 = AccountTotals(totals: [
			AccountID("test.example.1")! : 10.0,
			AccountID("test.example.2")! : 50.0,
			AccountID("example.3")! : 99.0,
		])
		let totals2 = AccountTotals(totals: [
			AccountID("example.1")! : 10.0,
			AccountID("example.2")! : 50.0,
			AccountID("example.3")! : 100.0,
		])
		#expect(totals1 + totals2 == totals2 + totals1)
		#expect((totals1 + totals2).debugDescription == """
			example.1 10,00
			example.2 50,00
			example.3 199,00
			test.example.1 10,00
			test.example.2 50,00
			"""
		)
	}
}
