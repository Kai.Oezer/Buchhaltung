// Copyright 2022-2025 Kai Oezer

import Testing
@testable import Buchhaltung
import TOMLike
import IssueCollection

@Suite
struct CompanyTests
{
	@Test("BH encoding of Seat", .tags(.bhCoding))
	func seatBHEncoding() throws
	{
		#expect(TestData.companySeat1.description == "c/o Hansi, Ballindamm 2, 20095 Hamburg, Deutschland")
		let archivedSeat = try TOMLikeCoder.encode(TestData.companySeat1)
		#expect(archivedSeat == TestData.archivedCompanySeat1)
	}

	@Test("BH decoding of Seat", .tags(.bhCoding))
	func seatBHDecoding() throws
	{
		var decodingIssues = IssueCollection()
		let decodedSeat = try TOMLikeCoder.decode(Company.Seat.self, from: TestData.archivedCompanySeat1, issues: &decodingIssues)
		#expect(decodedSeat == TestData.companySeat1)
		#expect(decodingIssues.count == 0)

		let decodedSeatWithInvalidZipcode = try TOMLikeCoder.decode(Company.Seat.self, from: TestData.archivedCompanySeatWithInvalidZipcode, issues: &decodingIssues)
		#expect(decodedSeatWithInvalidZipcode.zipCode == nil)
		#expect(decodingIssues.count == 1)
	}

	@Test("BH encoding of Company", .tags(.bhCoding))
	func bhEncoding() throws
	{
		let archivedCompany1 = try TOMLikeCoder.encode(TestData.company1)
		#expect(archivedCompany1 == TestData.archivedCompany1)
		let archivedCompany2 = try TOMLikeCoder.encode(TestData.company2)
		#expect(archivedCompany2 == TestData.archivedCompany2)
		let archivedCompany3 = try TOMLikeCoder.encode(TestData.company3)
		#expect(archivedCompany3 == TestData.archivedCompany3)
	}

	@Test("BH decoding of Company", .tags(.bhCoding))
	func bhDecoding() throws
	{
		var decodingIssues = IssueCollection()
		let decodedCompany1 = try TOMLikeCoder.decode(Company.self, from: TestData.archivedCompany1, issues: &decodingIssues)
		#expect(decodedCompany1 == TestData.company1)
		let decodedCompany2 = try TOMLikeCoder.decode(Company.self, from: TestData.archivedCompany2, issues: &decodingIssues)
		#expect(decodedCompany2 == TestData.company2)
		let decodedCompany3 = try TOMLikeCoder.decode(Company.self, from: TestData.archivedCompany3, issues: &decodingIssues)
		#expect(decodedCompany3 == TestData.company3)
	}

	@Test
	func shareholder()
	{
		let shareholder = Company.Shareholder(name: "Zimmermann", personFirstName: "Eberhardt", personTaxID: "01234567890", entry: Day(2024,2,10)!, guaranteedAmount: 10_000)
		#expect(shareholder.isMissingEntries == false)
		var s1 = shareholder
		s1.name = ""
		#expect(s1.isMissingEntries)
		var s2 = shareholder
		s2.personFirstName = ""
		#expect(s2.isMissingEntries)
		var s3 = shareholder
		s3.personTaxID = ""
		#expect(s3.isMissingEntries)
		var s4 = shareholder
		s4.entry = .distantFuture
		#expect(s4.isMissingEntries)
	}

	@Test
	func executive()
	{
		let executive = Company.Executive(id: 1, name: "Eberhardt Zimmermann", address: "Alexanderplatz 1, 10178, Berlin, Germany")
		#expect(executive.isMissingEntries == false)
		var e1 = executive
		e1.name = ""
		#expect(e1.isMissingEntries)
		var e2 = executive
		e2.address = ""
		#expect(e2.isMissingEntries)
	}
}
