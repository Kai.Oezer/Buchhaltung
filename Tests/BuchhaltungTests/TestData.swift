// Copyright 2021-2025 Kai Oezer
// swiftlint:disable file_length type_body_length

import Foundation
@testable import Buchhaltung
import DEGAAP

struct TestData
{
	static var simpleRecord : JournalRecord {
		JournalRecord(
			date: Day(2020, 10, 13)!,
			title: "HP LaserJet printer",
			debit: [Record.Entry(account: TestData.skr03_equipment_office, value: 119)],
			credit: [Record.Entry(account: TestData.skr03_bank, value: 119)]
		)
	}

	static let archivedSimpleRecord =
		"""
		chart SKR03

		2020-10-13
		"HP LaserJet printer"
		d 0420 119,00
		c 1200 119,00
		"""

	static var compoundRecord : JournalRecord {
		JournalRecord(
			date: Day(2020, 10, 13)!,
			title: "purchase of HP LaserJet printer",
			debit: [
				Record.Entry(account: TestData.skr03_equipment_office, value: 100),
				Record.Entry(account: TestData.skr03_receivable_vat_19, value: 19)
			],
			credit: [Record.Entry(account: TestData.skr03_bank, value: 119)]
		)
	}

	static let archivedCompoundRecord =
		"""
		chart SKR03

		2020-10-13
		"purchase of HP LaserJet printer"
		d 0420 100,00
		d 1576 19,00
		c 1200 119,00
		"""

	static let archivedRecord_tags =
		"""
		chart SKR03

		2023-07-10
		"Computer Vision Test Machine"
		tags "#101", "Building 5", "Project GrandSlam"
		d 0420 2.000,00
		d 1576 380,00
		c 1200 2.380,00
		"""

	static let archivedRecord_tags_note_links =
		"""
		chart SKR04

		2023-07-10
		"Computer Vision Test Machine"
		note "Payment is evenly split with our project partner."
		tags "#101", "Building 5", "Project GrandSlam"
		link "file://Volumes/Accounting/Receipts/2023/07/star_computer_23455.pdf"
		link "file://Volumes/Legal/Agreements/VisionProject.pdf"
		d 0650 2.000,00
		d 1406 380,00
		c 1800 2.380,00
		"""

	static let archivedJournal_startingACompany =
		"""
		chart SKR03

		2019-11-06
		"Bank deposit for subscribed capital of company"
		d 1200 7.000,00
		c 0800 7.000,00

		2019-11-07
		"lawyer and notarization expenses"
		d 1576 10,45
		d 4950 55,00
		c 1200 65,45

		2019-11-13
		"HP LaserJet printer"
		d 1576 19,00
		d 0420 100,00
		c 1200 119,00

		2019-11-13
		"HP LaserJet printer warranty extension insurance"
		d 4360 12,00
		c 1200 12,00

		2019-11-15
		"business registration (Handelsregisteranmeldung)"
		d 2300 150,00
		c 1200 150,00

		2019-11-26
		"business registration (Gewerbeanmeldung)"
		d 2300 15,00
		c 1200 15,00
		"""

	/// Sachanlage Kauf, Abschreibung, Verkauf (mit Gewinn)
	static let archivedJournal_buyAndSellFixedAsset_IKR =
		"""
		chart IKR

		2018-08-14
		"LKW Einkauf fuer 120.000 Euro Netto - Abschreibbar auf 5 Jahre"
		d 0840 120.000,00
		d 2600 22.800,00
		c 2800 142.800,00

		2018-12-31
		"Abschreibung auf LKW nach erstem Jahr (5 Monate)"
		d 6520 10.000,00
		c 0840 10.000,00

		2019-12-31
		"Abschreibung auf LKW nach zweitem Jahr"
		d 6520 24.000,00
		c 0840 24.000,00

		2020-12-31
		"Abschreibung auf LKW nach drittem Jahr"
		d 6520 24.000,00
		c 0840 24.000,00

		2021-05-10
		"Letzte Abschreibung auf LKW vor Verkauf im vierten Jahr (im 5. Monat)"
		d 6520 10.000,00
		c 0840 10.000,00

		2021-05-10
		"Minderung des Ertrages um den Buchwert, Minderung des Wertes des Fuhrparks."
		d 5460 52.000,00
		c 0840 52.000,00

		2021-05-10
		"Verkauf des LKW fuer 55.000 Euro Netto (aktueller Buchwert: 52.000 Euro)"
		d 2800 65.450,00
		c 5410 55.000,00
		c 4800 10.450,00
		"""

	/// 20% Sonderabschreibung auf Investitionsgüter für kleine Betriebe (Betriebsvermögen unter 235.000)
	/// [§ 7g Abs. 5 u. 6 EStG](https://www.gesetze-im-internet.de/estg/__7g.html)
	static let archivedJournal_Sonderabschreibung =
		"""
		chart SKR03

		2020-06-10
		"Apple Pro Display XDR"
		d 0420 4.500,00
		d 1576 855,00
		c 1200 5.355,00

		2020-12-31
		"reguläre Abschreibung (33,33 % p.a.) für 2020"
		d 4830 875,00
		c 0420 875,00

		2021-12-31
		"reguläre Abschreibung (33,33 % p.a.) für 2021"
		d 4830 1500,00
		c 0420 1.500,00

		2021-12-31
		"Sonderabschreibung (20 %) nach Ende des Jahres nach der Anschaffung"
		d 4851 900,00
		c 0420 900,00

		2022-12-31
		"reguläre restliche Abschreibung (bis zu 33,33 % p.a.) für 2022"
		d 4830 1200,00
		c 0420 1.225,00

		2022-12-31
		"Erinnerungswert - weil der Monitor weiterhin genutzt wird"
		d 0420 1,00
		c 2310 1,00
		"""

	static let archivedJournal_selling_SKR03 =
		"""
		chart SKR03

		2020-04-05
		"Verkauf auf Rechnung, Aufteilung der Forderung in Erlös und Umsatzsteuer"
		d 1400 645,00
		c 8000 542,00
		c 1770 103,00

		2020-04-08
		"Eingang der Zahlung vom Kunden in das Bankkonto"
		d 1200 645,00
		c 1400 645,00
		"""

	static var journal_nested_groups : Journal {
		let records : [JournalRecord] = [
			JournalRecord(
				date: Day(2022,9,23)!,
				title: "Zahlungsforderung an Kunde B",
				debit: [Record.Entry(account: DEGAAPSourceChartItemID("1410")!, value: 48_790)],
				credit: [
					Record.Entry(account: DEGAAPSourceChartItemID("8400")!, value: 41_000),
					Record.Entry(account: DEGAAPSourceChartItemID("1776")!, value: 7_790)
				]
			),
			JournalRecord(date: Day(2022,9,26)!, title: "Gehälter September") {
				JournalRecord(
					date: Day(2022,9,26)!,
					title: "Gehälter September",
					debit: [Record.Entry(account: DEGAAPSourceChartItemID("4120")!, value: 15_000)],
					credit: [Record.Entry(account: DEGAAPSourceChartItemID("1755")!, value: 15_000)]
				)
				JournalRecord(
					date: Day(2022,9,27)!,
					title: "Lohnsteuerzahlungen an Finanzamt",
					debit: [Record.Entry(account: DEGAAPSourceChartItemID("1741")!, value: 2_700)],
					credit: [Record.Entry(account: DEGAAPSourceChartItemID("1200")!, value: 2_700)]
				)
				JournalRecord(date: Day(2022,9,27)!, title: "Mitarbeiter 1") {
					JournalRecord(
						date: Day(2022,9,27)!,
						title: "Gehaltsverrechnung für Mitarbeiter 1",
						debit: [Record.Entry(account: DEGAAPSourceChartItemID("1755")!, value: 4_500)],
						credit: [
							Record.Entry(account: DEGAAPSourceChartItemID("1740")!, value: 2_800),
							Record.Entry(account: DEGAAPSourceChartItemID("1741")!, value: 1_200),
							Record.Entry(account: DEGAAPSourceChartItemID("1742")!, value: 500),
						]
					)
					JournalRecord(
						date: Day(2022,9,27)!,
						title: "Gehaltszahlung an Mitarbeiter 1",
						debit: [Record.Entry(account: DEGAAPSourceChartItemID("1740")!, value: 2_800)],
						credit: [Record.Entry(account: DEGAAPSourceChartItemID("1200")!, value: 2_800)]
					)
				}
				JournalRecord(date: Day(2022,9,27)!, title: "Mitarbeiter 2") {
					JournalRecord(
						date: Day(2022,9,27)!,
						title: "Gehaltsverrechnung für Mitarbeiter 2",
						debit: [Record.Entry(account: DEGAAPSourceChartItemID("1755")!, value: 6_000)],
						credit: [
							Record.Entry(account: DEGAAPSourceChartItemID("1740")!, value: 3_500),
							Record.Entry(account: DEGAAPSourceChartItemID("1741")!, value: 1_500),
							Record.Entry(account: DEGAAPSourceChartItemID("1742")!, value: 1_000),
						]
					)
					JournalRecord(
						date: Day(2022,9,27)!,
						title: "Gehaltszahlung an Mitarbeiter 2",
						debit: [Record.Entry(account: DEGAAPSourceChartItemID("1740")!, value: 3_500)],
						credit: [Record.Entry(account: DEGAAPSourceChartItemID("1200")!, value: 3_500)]
					)
				}
				JournalRecord(
					date: Day(2022,9,27)!,
					title: "Sozialabgaben an Krankenkassen",
					debit: [Record.Entry(account: DEGAAPSourceChartItemID("1742")!, value: 1_500)],
					credit: [Record.Entry(account: DEGAAPSourceChartItemID("1200")!, value: 1_500)]
				)
			},
			JournalRecord(
				date: Day(2022,10,5)!,
				title: "Zahlungseingang von Kunde B",
				debit: [Record.Entry(account: DEGAAPSourceChartItemID("1200")!, value: 48_790)],
				credit: [Record.Entry(account: DEGAAPSourceChartItemID("1410")!, value: 48_790)]
			)
		]
		return Journal(records: records, chart: .skr03)
	}

	static let archivedJournal_nested_groups =
		"""
		chart SKR03

		2022-09-23
		"Zahlungsforderung an Kunde B"
		d 1410 48.790,00
		c 8400 41.000,00
		c 1776 7.790,00

		2022-09-26
		group "Gehälter September"

		    2022-09-26
		    "Gehälter September"
		    d 4120 15.000,00
		    c 1755 15.000,00

		    2022-09-27
		    "Lohnsteuerzahlungen an Finanzamt"
		    d 1741 2.700,00
		    c 1200 2.700,00

		    2022-09-27
		    group "Mitarbeiter 1"

		        2022-09-27
		        "Gehaltsverrechnung für Mitarbeiter 1"
		        d 1755 4.500,00
		        c 1740 2.800,00
		        c 1741 1.200,00
		        c 1742 500,00

		        2022-09-27
		        "Gehaltszahlung an Mitarbeiter 1"
		        d 1740 2.800,00
		        c 1200 2.800,00

		    close # Mitarbeiter 1

		    2022-09-27
		    group "Mitarbeiter 2"

		        2022-09-27
		        "Gehaltsverrechnung für Mitarbeiter 2"
		        d 1755 6.000,00
		        c 1740 3.500,00
		        c 1741 1.500,00
		        c 1742 1.000,00

		        2022-09-27
		        "Gehaltszahlung an Mitarbeiter 2"
		        d 1740 3.500,00
		        c 1200 3.500,00

		    close # Mitarbeiter 2

		    2022-09-27
		    "Sozialabgaben an Krankenkassen"
		    d 1742 1.500,00
		    c 1200 1.500,00

		close # Gehälter September

		2022-10-05
		"Zahlungseingang von Kunde B"
		d 1200 48.790,00
		c 1410 48.790,00
		"""

	static var journal_flattened_groups : Journal {
		let records : [JournalRecord] = [
			JournalRecord(
				date: Day(2022,9,23)!,
				title: "Zahlungsforderung an Kunde B",
				debit: [Record.Entry(account: DEGAAPSourceChartItemID("1410")!, value: 48_790)],
				credit: [
					Record.Entry(account: DEGAAPSourceChartItemID("8400")!, value: 41_000),
					Record.Entry(account: DEGAAPSourceChartItemID("1776")!, value: 7_790)
				]
			),
			JournalRecord(
				date: Day(2022,9,26)!,
				title: "Gehälter September",
				debit: [Record.Entry(account: DEGAAPSourceChartItemID("4120")!, value: 15_000)],
				credit: [Record.Entry(account: DEGAAPSourceChartItemID("1755")!, value: 15_000)]
			),
			JournalRecord(
				date: Day(2022,9,27)!,
				title: "Lohnsteuerzahlungen an Finanzamt",
				debit: [Record.Entry(account: DEGAAPSourceChartItemID("1741")!, value: 2_700)],
				credit: [Record.Entry(account: DEGAAPSourceChartItemID("1200")!, value: 2_700)]
			),
			JournalRecord(
				date: Day(2022,9,27)!,
				title: "Gehaltsverrechnung für Mitarbeiter 1",
				debit: [Record.Entry(account: DEGAAPSourceChartItemID("1755")!, value: 4_500)],
				credit: [
					Record.Entry(account: DEGAAPSourceChartItemID("1740")!, value: 2_800),
					Record.Entry(account: DEGAAPSourceChartItemID("1741")!, value: 1_200),
					Record.Entry(account: DEGAAPSourceChartItemID("1742")!, value: 500),
				]
			),
			JournalRecord(
				date: Day(2022,9,27)!,
				title: "Gehaltszahlung an Mitarbeiter 1",
				debit: [Record.Entry(account: DEGAAPSourceChartItemID("1740")!, value: 2_800)],
				credit: [Record.Entry(account: DEGAAPSourceChartItemID("1200")!, value: 2_800)]
			),
			JournalRecord(
				date: Day(2022,9,27)!,
				title: "Gehaltsverrechnung für Mitarbeiter 2",
				debit: [Record.Entry(account: DEGAAPSourceChartItemID("1755")!, value: 6_000)],
				credit: [
					Record.Entry(account: DEGAAPSourceChartItemID("1740")!, value: 3_500),
					Record.Entry(account: DEGAAPSourceChartItemID("1741")!, value: 1_500),
					Record.Entry(account: DEGAAPSourceChartItemID("1742")!, value: 1_000),
				]
			),
			JournalRecord(
				date: Day(2022,9,27)!,
				title: "Gehaltszahlung an Mitarbeiter 2",
				debit: [Record.Entry(account: DEGAAPSourceChartItemID("1740")!, value: 3_500)],
				credit: [Record.Entry(account: DEGAAPSourceChartItemID("1200")!, value: 3_500)]
			),
			JournalRecord(
				date: Day(2022,9,27)!,
				title: "Sozialabgaben an Krankenkassen",
				debit: [Record.Entry(account: DEGAAPSourceChartItemID("1742")!, value: 1_500)],
				credit: [Record.Entry(account: DEGAAPSourceChartItemID("1200")!, value: 1_500)]
			),
			JournalRecord(
				date: Day(2022,10,5)!,
				title: "Zahlungseingang von Kunde B",
				debit: [Record.Entry(account: DEGAAPSourceChartItemID("1200")!, value: 48_790)],
				credit: [Record.Entry(account: DEGAAPSourceChartItemID("1410")!, value: 48_790)]
			)
		]
		return Journal(records: records, chart: .skr03)
	}

	// MARK: -

	static let balanceSheet1 : BalanceSheet = {
		var bs = BalanceSheet()
		bs.assets.fixedAssets = 3439
		bs.assets.currentAssets = 12310.10
		bs.assets.deferrals = Deferrals(monetaryValue: .zero)
		bs.assets.deferredTax = DeferredTax(monetaryValue: 450.80)
		bs.assets.surplus = SurplusFromOffsetting(monetaryValue: 0)
		bs.eqLiab.equity = 10530.10
		bs.eqLiab.provisions = 1500
		bs.eqLiab.liabilities = 4610.30
		bs.eqLiab.deferrals = Deferrals(monetaryValue: 0)
		bs.eqLiab.deferredTax = DeferredTax(monetaryValue: 0)
		return bs
	}()

	static let balanceSheet2 : BalanceSheet = {
		var bs = BalanceSheet()
		bs.assets.fixedAssets = 504.30
		bs.assets.currentAssets = 12832
		bs.assets.deferrals = Deferrals(monetaryValue: 10)
		bs.assets.deferredTax = DeferredTax(monetaryValue: 120.40)
		bs.assets.surplus = SurplusFromOffsetting(monetaryValue: 0)
		bs.previousPeriodAssets.fixedAssets = 3439
		bs.previousPeriodAssets.currentAssets = 12310.10
		bs.previousPeriodAssets.deferrals = Deferrals(monetaryValue: .zero)
		bs.previousPeriodAssets.deferredTax = DeferredTax(monetaryValue: 450.80)
		bs.previousPeriodAssets.surplus = SurplusFromOffsetting(monetaryValue: 0)
		bs.eqLiab.equity = 14100
		bs.eqLiab.provisions = 2310
		bs.eqLiab.liabilities = 3721.30
		bs.eqLiab.deferrals = Deferrals(monetaryValue: 100)
		bs.eqLiab.deferredTax = DeferredTax(monetaryValue: 540)
		bs.previousPeriodEqLiab.equity = 10530.10
		bs.previousPeriodEqLiab.provisions = 1500
		bs.previousPeriodEqLiab.liabilities = 4610.30
		bs.previousPeriodEqLiab.deferrals = Deferrals(monetaryValue: 0)
		bs.previousPeriodEqLiab.deferredTax = DeferredTax(monetaryValue: 0)
		return bs
	}()

	static let archivedBalanceSheet1 =
		"""
		asset fixed 3.439,00
		asset current 12.310,10
		asset deferrals 0,00
		asset deferred_tax 450,80
		asset surplus 0,00
		eqlia equity 10.530,10
		eqlia provisions 1.500,00
		eqlia liabilities 4.610,30
		eqlia deferrals 0,00
		eqlia deferred_tax 0,00
		"""

	static let archivedBalanceSheet2 =
		"""
		asset fixed 504,30 3.439,00
		asset current 12.832,00 12.310,10
		asset deferrals 10,00
		asset deferred_tax 120,40 450,80
		asset surplus 0,00
		eqlia equity 14.100,00 10.530,10
		eqlia provisions 2.310,00 1.500,00
		eqlia liabilities 3.721,30 4.610,30
		eqlia deferrals 100,00
		eqlia deferred_tax 540,00
		"""

	// MARK: -

	static let companySeat1 = Company.Seat(
		street: "Ballindamm",
		houseNo: "2",
		zipCode: "20095",
		cityOrRegion: "Hamburg",
		country: "Deutschland",
		note: "c/o Hansi"
	)

	static let archivedCompanySeat1 =
		"""
		seat note c/o Hansi
		seat street Ballindamm
		seat houseno 2
		seat zipcode 20095
		seat city Hamburg
		seat country Deutschland
		"""

	static let archivedCompanySeatWithInvalidZipcode =
		"""
		seat note c/o Hansi
		seat street Ballindamm
		seat houseno 2
		seat zipcode X
		seat city Hamburg
		seat country Deutschland
		"""

	static let companySeat2 = Company.Seat(
		street: "Hackescher Markt",
		zipCode: "10178",
		cityOrRegion: "Berlin"
	)

	static let company1 : Company = {
		var company = Company()
		company.name = ["Sauber Hausverwaltung GmbH"]
		company.seat = companySeat2
		company.tradeRegistration.tradeCourt = "Amtsgericht Berlin-Mitte"
		company.tradeRegistration.tradeRegistrationID = "HRB 212121"
		company.subscribedCapital = 32_000
		company.chiefExecutives = [Company.Executive(name: "Walter Ramazotti", address: "Potsdamer Platz 1, Berlin")]
		company.size = .small
		company.numberOfEmployees = 23
		return company
	}()

	static let archivedCompany1 =
		"""
		name Sauber Hausverwaltung GmbH
		seat street Hackescher Markt
		seat zipcode 10178
		seat city Berlin
		seat country Deutschland
		form GmbH
		regc Amtsgericht Berlin-Mitte
		regn HRB 212121
		scap 32.000,00
		ceo- Walter Ramazotti; Potsdamer Platz 1, Berlin
		size small
		empl 23
		liqu no
		"""

	static let company2 : Company = {
		var company = Company()
		company.name = ["Luftikus GmbH"]
		company.logo = "/Corporate Identity/logo.png"
		company.seat = .init(street:"Luftblasenstrasse", houseNo: "1230", zipCode: "74072", cityOrRegion: "Heilbronn", country: "Deutschland", note: "c/o Hans Schwebend")
		company.tradeRegistration.tradeCourt = "Amtsgericht Luftschloss"
		company.tradeRegistration.tradeRegistrationID = "HRB 999666999"
		company.subscribedCapital = 40_000
		company.chiefExecutives = [Company.Executive(name: "Hans Schwebend", address: "74072 Heilbronn")]
		company.size = .tiny
		company.numberOfEmployees = 0
		company.inLiquidation = true
		return company
	}()

	static let archivedCompany2 =
		"""
		name Luftikus GmbH
		logo "/Corporate Identity/logo.png"
		seat note c/o Hans Schwebend
		seat street Luftblasenstrasse
		seat houseno 1230
		seat zipcode 74072
		seat city Heilbronn
		seat country Deutschland
		form GmbH
		regc Amtsgericht Luftschloss
		regn HRB 999666999
		scap 40.000,00
		ceo- Hans Schwebend; 74072 Heilbronn
		size tiny
		empl 0
		liqu yes
		"""

	static let companyShareholder1 = Company.Shareholder(
		id: 0,
		name: "Goldfinger",
		personFirstName: "Auric",
		personTaxID: "12345678901",
		entry: Day(2012, 6, 12)!,
		exit: .distantFuture,
		guaranteedAmount: 100_000)

	static let companyShareholder2 = Company.Shareholder(
		id: 1,
		name: "Trevelyan",
		personFirstName: "Alec",
		personTaxID: "10987654321",
		entry: Day(2008, 10, 15)!,
		exit: Day(2012, 06, 12)!,
		guaranteedAmount: 80_000)

	static let company3 : Company = {
		var company = Company()
		company.name = ["Universal Exports Ltd."]
		company.seat = .init(street:"Marienstrasse", houseNo: "340", zipCode: "30171", cityOrRegion: "Hannover", country: "Deutschland")
		company.legalForm = .ug
		company.business = "Troubleshooting"
		company.tradeRegistration.tradeCourt = "Amtsgericht Hannover"
		company.tradeRegistration.tradeRegistrationID = "HRB 1100711"
		company.fiscalRegistration.st13 = "1234567890123"
		company.fiscalRegistration.bf4 = "1112"
		company.subscribedCapital = 350_000
		company.chiefExecutives = [Company.Executive(name: "Ernst Blofeld", address: "80939 München")]
		company.size = .medium
		company.numberOfEmployees = 1007
		company.inLiquidation = true
		company.shareholders = [
			companyShareholder1,
			companyShareholder2
		]
		company.contingentLiabilities = [
			Company.ContingentLiability(type: .associatedCompanies, amount: 23_100, note: "Guarantees for transportation of precious metals.")
		]
		company.executiveLoans = [
			Company.ExecutiveLoan(name: "Auric Goldfinger", advancePayments: 12_500, creditsGranted: 2_300),
			Company.ExecutiveLoan(name: "Alec Trevelyan", liabilities: 10_500.80)
		]
		return company
	}()

	static let archivedCompany3 =
		"""
		name Universal Exports Ltd.
		seat street Marienstrasse
		seat houseno 340
		seat zipcode 30171
		seat city Hannover
		seat country Deutschland
		form UG
		does Troubleshooting
		regc Amtsgericht Hannover
		regn HRB 1100711
		scap 350.000,00
		st13 1234567890123
		bf4- 1112
		ceo- Ernst Blofeld; 80939 München
		shrh Goldfinger; ; Auric; 12345678901; 100.000,00; 2012-06-12
		shrh Trevelyan; ; Alec; 10987654321; 80.000,00; 2008-10-15; 2012-06-12
		size medium
		empl 1007
		liqu yes
		liab assc; 23.100,00; Guarantees for transportation of precious metals.
		loan Auric Goldfinger; advances 12.500,00; credits 2.300,00
		loan Alec Trevelyan; liabilities 10.500,80
		"""

	// MARK: -

	static let notesBelowBalanceSheet1 : NotesBelowBalanceSheet = {
		var notes = NotesBelowBalanceSheet()
		notes.companyInformation = company1
		notes.incomeCalculationMethod = .gkv
		return notes
	}()

	static let archivedNotesBelowBalanceSheet1 =
		"""
		\(archivedCompany1)
		meth GKV
		"""

	static let notesBelowBalanceSheet2 : NotesBelowBalanceSheet = {
		var notes = NotesBelowBalanceSheet()
		notes.companyInformation = company2
		notes.incomeCalculationMethod = .ukv
		return notes
	}()

	static let archivedNotesBelowBalanceSheet2 =
		"""
		\(archivedCompany2)
		meth UKV
		"""

// MARK: -

	static let fixedAssets1 : [Assets.FixedAsset] = [
		.init(registrationID: "1", type: .equipment, name: "computer display", acquisitionDate: Day(2020, 4, 13)!, acquisitionCost: 249, depreciationSchedule: .init(depreciationYears: 5)),
		.init(registrationID: "1a", type: .equipment, name: "HDMI cable", acquisitionDate: Day(2020, 6, 9)!, acquisitionCost: 20, depreciationSchedule: .init(depreciationYears: 7))
	]

	static let fixedAssets2 : [Assets.FixedAsset] = [
		.init(registrationID: "4", type: .vehicle, name: "VW Caddy TDI DSG", acquisitionDate: Day(2020, 5, 29)!, acquisitionCost: 32540, depreciationSchedule: .init(depreciationYears: 6), retirementDate: Day(2028, 10, 30)!),
		.init(registrationID: "5", type: .equipment, name: "iPhone test device", acquisitionDate: Day(2020, 7, 4)!, acquisitionCost: 889, depreciationSchedule: .init(depreciationYears: 5)),
		.init(registrationID: "11F", type: .equipment, name: "aerial drone", acquisitionDate: Day(2020, 8, 13)!, acquisitionCost: 750, depreciationSchedule: .init(progression: .gwg))
	]

	static let currentAssets1 : [Assets.CurrentAsset] = [
		.init(type: .receivable, name: "Forderung für verkaufte Software", value:2500)
	]

	static let archivedAssets1 =
		"""
		= 1
		equipment
		"computer display"
		acqd 2020-04-13
		cost 249,00
		depr linear 5
		= 1a
		equipment
		"HDMI cable"
		acqd 2020-06-09
		cost 20,00
		depr linear 7
		"""

	static let archivedAssets2 =
		"""
		= 5
		equipment
		"iPhone test device"
		acqd 2020-07-04
		cost 889,00
		depr linear 5
		= 11F
		equipment
		"aerial drone"
		acqd 2020-08-13
		cost 750,00
		depr gwg
		= 4
		vehicle
		"VW Caddy TDI DSG"
		acqd 2020-05-29
		retd 2028-10-30
		cost 32.540,00
		depr linear 6
		+
		receivable
		"Forderung für verkaufte Software"
		valu 2.500,00
		"""

	static let archivedAssets3 =
		"""
		= 2
		equipment
		"server rack"
		acqd 2015-11-14
		retd 2020-02-28
		cost 1.200,00
		depr linear 10
		= 14
		equipment
		"web server hardware"
		acqd 2018-05-10
		cost 3.600,00
		depr linear 3
		"""

	static let archivedAssets4 =
		"""
		= 2
		equipment
		"server rack"
		acqd 2015-11-14
		retd 2020-03-05
		cost 1.200,00
		depr linear 10
		= 14
		equipment
		"web server hardware"
		acqd 2018-05-10
		cost 3.600,00
		depr linear 3
		= 15
		equipment
		"tablet computer"
		acqd 2020-04-04
		cost 560,00
		depr gwg
		"""

	// MARK: -

	static let debts1 : [Debt] = [
		.init(name: "Kredit von der Bank, 5 % Zinsen", loan: .init(date: Day(2019, 1, 15)!, value: 10_000), payments: [
			.init(date: Day(2019, 7, 14)!, value: 5166),
			.init(date: Day(2020, 1, 14)!, value: 5166),
			.init(date: Day(2020, 7, 14)!, value: 5166)
		])
	]

	static let archivedDebts1 =
		"""
		-
		"Kredit von der Bank, 5 % Zinsen"
		loan 2019-01-15 10.000,00
		paym 2019-07-14 5.166,00
		paym 2020-01-14 5.166,00
		paym 2020-07-14 5.166,00
		"""

	static let archivedDebts2 =
		"""
		-
		"Kredit 1"
		loan 2022-06-01 45.000,00
		paym 2023-06-01 20.000,00
		paym 2023-12-31 25.000,00
		-
		"Gesellschafterdarlehen"
		loan 2022-10-15 12.000,00
		paym 2023-02-01 12.000,00
		-
		"Kredit 2"
		loan 2021-08-10 10.000,00
		paym 2022-02-09 5.000,00
		paym 2022-08-09 5.000,00
		-
		"Kredit 3"
		loan 2022-07-18 50.000,00
		paym 2023-12-31 51.000,00
		"""

	// MARK: -

	static let incomeStatement1 : IncomeStatement = {
		var statement = IncomeStatement()
		statement.results.earnings = 12400
		statement.results.otherEarnings = 100.80
		statement.results.materialCosts = 680.90
		statement.results.salaries = 34200
		statement.results.depreciations = 0
		statement.results.otherExpenses = 120.10
		statement.results.tax = 840.10
		return statement
	}()

	static let incomeStatement2 : IncomeStatement = {
		var statement = IncomeStatement()
		statement.results.earnings = 14000
		statement.results.otherEarnings = 1205
		statement.results.materialCosts = 450
		statement.results.salaries = 35400
		statement.results.depreciations = 320
		statement.results.otherExpenses = 2400
		statement.results.tax = 1960
		statement.previousYearResults.earnings = 12400
		statement.previousYearResults.otherEarnings = 100.80
		statement.previousYearResults.materialCosts = 680.90
		statement.previousYearResults.salaries = 34200
		statement.previousYearResults.depreciations = 0
		statement.previousYearResults.otherExpenses = 120.10
		statement.previousYearResults.tax = 840.10
		return statement
	}()

	static let archivedIncomeStatement1 =
		"""
		earnings 12.400,00
		earnings_other 100,80
		material 680,90
		salaries 34.200,00
		depreciations 0,00
		expenses 120,10
		tax 840,10
		"""

	static let archivedIncomeStatement2 =
		"""
		earnings 14.000,00 12.400,00
		earnings_other 1.205,00 100,80
		material 450,00 680,90
		salaries 35.400,00 34.200,00
		depreciations 320,00
		expenses 2.400,00 120,10
		tax 1.960,00 840,10
		"""

	// MARK: -

	static let inventory1 : Inventory = {
		var assets = Assets()
		assets.currentAssets = Set([
			.init(type:.finishedGoods, name:"Getriebe", value:650),
			.init(type:.receivable, name:"Forderungen", value:1800),
			.init(type:.cashEquivalent, name:"Kasse", value:15300)
		])
		assets.fixedAssets = Set([
			Assets.FixedAsset(
				registrationID: "M105",
				type:.machinery,
				name:"Stanzmaschine",
				acquisitionDate: Day(2020, 5, 10)!,
				acquisitionCost: 8000,
				depreciationSchedule: .init(depreciationYears: 10)
				)
		])
		let debts = Debts(debts: [
			Debt(name:"Materialbeschaffung",
				loan: .init(date: Day(2020, 11, 20)!, value:1000),
				payments: [
					.init(date:Day(2020, 12, 20)!, value:500),
					.init(date:Day(2021, 1, 20)!, value:500)
				])
		])
		return Inventory(period: .year(2020)!, assets: assets, debts: debts)
	}()

	static let archivedInventory1 =
		"""
		2020-12-31
		fixd machinery 7.466,72
		curr goods 650,00
		curr receivable 1.800,00
		curr cash 15.300,00
		debt short 500,00
		"""

	// MARK: -

	static let assetChangesStatement1 : AssetChangesStatement = {
		var changes = AssetChangesStatement()
		var accountChanges = AssetChangesStatement.AssetAccountChanges()

		accountChanges.acquisitionCosts.begin = 1240.20
		accountChanges.acquisitionCosts.new = 240.90
		accountChanges.acquisitionCosts.interestPaidForBorrowedCapital = 0
		accountChanges.acquisitionCosts.retirements = 0
		accountChanges.acquisitionCosts.rebookings = 0
		accountChanges.acquisitionCosts.end = 1481.10
		accountChanges.depreciations.begin = 102.40
		accountChanges.depreciations.new = 42.10
		accountChanges.depreciations.retirements = 35.50
		accountChanges.depreciations.end = 144.50
		accountChanges.bookValues.current = 1336.60
		accountChanges.bookValues.previous = 1137.80
		changes.changes[DEGAAPAccounts.ass_fixed_equipment_office] = accountChanges

		accountChanges.acquisitionCosts.begin = 560.20
		accountChanges.acquisitionCosts.new = 340.00
		accountChanges.acquisitionCosts.interestPaidForBorrowedCapital = 0
		accountChanges.acquisitionCosts.retirements = 0
		accountChanges.acquisitionCosts.rebookings = 0
		accountChanges.acquisitionCosts.end = 900.20
		accountChanges.depreciations.begin = 558.20
		accountChanges.depreciations.new = 339.00
		accountChanges.depreciations.retirements = 0
		accountChanges.depreciations.end = 897.20
		accountChanges.bookValues.current = 3
		accountChanges.bookValues.previous = 2
		changes.changes[DEGAAPAccounts.ass_fixed_equipment_gwg] = accountChanges

		return changes
	}()

	static let archivedAssetChangesStatement1 =
		"""
		>>> bs.ass.fixAss.tan.otherEquipm.gwg
		acq begin 560,20
		acq new 340,00
		acq interestPaid 0,00
		acq retirements 0,00
		acq rebookings 0,00
		acq end 900,20
		dep begin 558,20
		dep new 339,00
		dep retirements 0,00
		dep end 897,20
		bkv current 3,00
		bkv previous 2,00
		<<<
		>>> bs.ass.fixAss.tan.otherEquipm.office
		acq begin 1.240,20
		acq new 240,90
		acq interestPaid 0,00
		acq retirements 0,00
		acq rebookings 0,00
		acq end 1.481,10
		dep begin 102,40
		dep new 42,10
		dep retirements 35,50
		dep end 144,50
		bkv current 1.336,60
		bkv previous 1.137,80
		<<<
		"""

	// MARK: -

	static let notes1 : Notes = {
		var result = Notes()
		result.assetChangesStatement = assetChangesStatement1
		return result
	}()

	static let archivedNotes1 =
		"""
		[asset_changes]
		\(archivedAssetChangesStatement1)
		"""

	// MARK: -

	static let accountTotals1 = AccountTotals(totals: [
		DEGAAPAccounts.ass_fixed_equipment_office : 8321.34,
		DEGAAPAccounts.ass_fixed_equipment_gwg : 8,
		DEGAAPAccounts.eqLiab_equity_subscribed : -25_500,
		DEGAAPAccounts.eqLiab_equity_retainedEarnings : -450
	])

	static let archivedAccountTotals1 =
		"""
		bs.ass.fixAss.tan.otherEquipm.gwg 8,00
		bs.ass.fixAss.tan.otherEquipm.office 8.321,34
		bs.eqLiab.equity.retainedEarnings.finalPrev -450,00
		bs.eqLiab.equity.subscribed.corp -25.500,00
		"""

	// MARK: -

	static var ledger1 : Ledger {
		let period = Period(start: Day(2019, 4, 1)!, end: Day(2019, 11, 1)!)
		let journal = Journal()
		let assets = Assets()
		var company = Company()
		company.name = ["Otto Normal GmbH"]
		company.seat = .init(zipCode: "20815", cityOrRegion:"Hamburg")
		company.tradeRegistration.tradeCourt = "Amtsgericht Hamburg"
		company.tradeRegistration.tradeRegistrationID = "XXX 112233"
		company.subscribedCapital = 25_000
		company.chiefExecutives = [Company.Executive(name: "Otto Normal", address: "Verbraucherstrasse 1, 20815 Hamburg")]
		company.numberOfEmployees = 5
		return Ledger(period: period, company: company, journal: journal, assets: assets)
	}

	static let archivedLedger1 =
		"""
		[format]
		1

		[period]
		2019-04-01 - 2019-11-01

		[taxonomy]
		6.3

		[company]
		name Otto Normal GmbH
		seat zipcode 20815
		seat city Hamburg
		seat country Deutschland
		form GmbH
		regc Amtsgericht Hamburg
		regn XXX 112233
		scap 25.000,00
		ceo- Otto Normal; Verbraucherstrasse 1, 20815 Hamburg
		size tiny
		empl 5
		liqu no

		[assets]


		[debts]


		[journal]
		chart SKR03
		"""

	static let archivedLedger2 =
		"""
		[format]
		1

		[period]
		2019-01-01 - 2019-12-31
		[balance_sheet_notes]
		regn -
		regl -
		regc -
		regn -
		liqu no
		micr -
		cred -
		depn -
		memb -

		[assets]


		[debts]


		[journal]

		"""

	static let archivedLedger3 =
		"""
		[format]
		1

		[period]
		2019-10-28 - 2019-12-31

		[company]
		name My Small Company
		seat street Ballindamm
		seat houseno 2
		seat zipcode 20095
		seat city Hamburg
		seat country Deutschland
		regc Hamburg-Mitte
		regn HRB 111111
		st13 1234567890123
		bf4- 1234
		scap 7.000,00
		ceo- Hansi Kleinmann; Ballindamm 2, 20095 Hamburg
		size tiny
		does Einzelhandel
		empl 3
		liqu no

		[assets]
		= 1
		equipment_office
		\"printer\"
		acqd 2019-11-13
		cost 108,40
		depr linear 3

		+
		cash
		"Bank"
		valu 6684,07

		[debts]
		-
		"Gesellschafterdarlehen für Registereintrag"
		loan 2019-11-09 150
		paym 2020-01-30 150

		[journal]
		chart SKR03

		2019-11-06
		\"Stammkapital Einzahlung 1/2\"
		d 1200 1.000,00
		c 0800 1.000,00

		2019-11-07
		\"Notarkosten\"
		d 4950 127,75
		d 1570 24,28
		c 1200 152,03

		2019-11-08
		\"Stammkapital Einzahlung 2/2\"
		d 1200 6.000,00
		c 0800 6.000,00

		2019-11-13
		\"printer\"
		tags \"#1\"
		d 0420 108,40
		d 1570 20,60
		c 1200 129,00

		2019-11-13
		\"Versicherung für printer\"
		d 4980 19,90
		c 1200 19,90

		2019-11-15
		\"Handelsregisteranmeldung\"
		d 4980 150,00
		c 1705 150,00

		2019-11-26
		\"Gewerbeanmeldung\"
		d 4980 15,00
		c 1200 15,00
		"""

	static let archivedLedger4 =
		"""
		[format]
		1

		[period]
		2020-01-01 - 2020-12-31

		[company]
		name My Small Company
		seat street Ballindamm
		seat houseno 2
		seat zipcode 20095
		seat city Hamburg
		seat country Deutschland
		regc Hamburg-Mitte
		regn HRB 111111
		scap 7.000,00
		ceo- Hansi Kleinmann; Ballindamm 2, 20095 Hamburg
		st13 1234567890123
		bf4- 1234
		size tiny
		does Einzelhandel
		empl 3
		liqu no

		[assets]
		= 1
		equipment_office
		\"Drucker\"
		acqd 2019-11-13
		cost 108,40
		depr linear 3
		= 2
		equipment
		\"Fahrrad\"
		acqd 2020-03-18
		cost 461,34
		depr gwg
		+
		cash
		\"Bank\"
		valu 5.720,27

		[journal]
		chart SKR03

		2020-01-07
		\"Geschäftsführergehalt für 2019\"
		d 4124 1,00
		c 1200 1,00

		2020-01-07
		\"Raumkosten 2019\"
		d 4200 6,39
		c 1200 6,39

		2020-01-07
		\"Kommunikationsgebühr\"
		d 4925 6,00
		c 1200 6,00

		2020-01-07
		\"Lizenzgebuehr\"
		d 4964 10,00
		c 1200 10,00

		2020-01-27
		\"Rückzahlung des Darlehens\"
		d 1705 150,00
		c 1200 150,00

		2020-01-30
		\"Umsatzsteuererstattung\"
		d 1200 44,88
		c 1576 44,88

		2020-03-03
		\"Betriebsausstattung\"
		d 4980 31,96
		d 1576 6,07
		c 1200 38,03

		2020-03-06
		\"Umsatzsteuererstattung\"
		d 1200 6,07
		c 1576 6,07

		2020-03-03
		\"Gebühr an Bundesanzeiger Verlag für Hinterlegung\"
		d 4957 48,30
		d 1576 9,18
		c 1200 57,48

		2020-03-13
		\"Arbeitsausrüstung\"
		d 4980 67,22
		d 1576 12,77
		c 1200 79,99

		2020-03-13
		\"Fahrrad\"
		d 0480 461,34
		d 1576 87,66
		c 1200 549,00

		2020-03-17
		\"Arbeitsausrüstung\"
		d 4980 101,25
		d 1576 19,23
		c 1200 120,48

		2020-03-19
		\"Arbeitsausrüstung\"
		d 4980 34,23
		d 1576 6,51
		c 1200 40,74

		2020-04-14
		\"Umsatzsteuererstattung\"
		d 1200 135,35
		c 1576 135,35

		2020-11-23
		\"IHK Mitgliedsbeitrag 2020\"
		d 4980 64,00
		c 1200 64,00

		2020-12-22
		\"Fachliteratur\"
		d 4940 25,70
		d 1570 1,29
		c 1200 26,99
		"""

	static let archivedLedger5 =
		"""
		[format]
		1

		[period]
		2021-01-01 - 2021-12-31

		[company]
		name My Small Company
		seat street Ballindamm
		seat houseno 2
		seat zipcode 20095
		seat city Hamburg
		seat country Deutschland
		regc Hamburg-Mitte
		regn HRB 111111
		scap 7.000,00
		ceo- Hansi Kleinmann; Ballindamm 2, 20095 Hamburg
		st13 1234567890123
		bf4- 1234
		size tiny
		does Einzelhandel
		empl 3
		liqu no

		[assets]
		= 1
		equipment_office
		"Drucker"
		acqd 2019-11-13
		cost 108,40
		depr linear 3
		= 2
		equipment
		"Fahrrad"
		acqd 2020-03-18
		cost 461,34
		depr gwg
		= 3
		equipment_office
		"Computer 1"
		acqd 2021-05-24
		retd 2021-09-02
		cost 738,66
		depr gwg
		= 4
		equipment_office
		"Computer 2"
		acqd 2021-09-07
		cost 738,66
		depr gwg
		+
		cash
		"Bank"
		valu 3.618,73

		[journal]
		chart SKR03

		2021-01-04
		"Computerzubehör"
		d 4980 150,39
		d 1576 24,06
		c 1200 174,45

		2021-01-08
		"Umsatzsteuererstattung vom Finanzamt"
		d 1200 25,35
		c 1576 25,35

		2021-01-27
		"Datenspeicher"
		d 4980 100,67
		d 1576 19,13
		c 1200 119,80

		2021-03-08
		"Lizenzgebuehr fuer Bildmarke"
		d 4964 60,00
		c 1200 60,00

		2021-03-08
		"Geschäftsführergehalt für 2020"
		d 4124 1,00
		c 1200 1,00

		2021-03-08
		"Gebühr für Mitbenutzung von Internet-Domain und Webhosting"
		d 4925 36,00
		c 1200 36,00

		2021-03-08
		"Miete/Raumkosten 2020"
		d 4222 36,00
		c 1200 36,00

		2021-04-09
		"Umsatzsteuererstattung vom Finanzamt"
		d 1200 19,13
		c 1576 19,13

		2021-05-24
		"Computer 1"
		d 0480 738,66
		d 1576 140,34
		c 1200 879,00

		2021-06-28
		"Computer 1 Zubehör"
		d 4980 113,45
		d 1576 21,55
		c 1200 135,00

		2021-07-14
		"Umsatzsteuererstattung vom Finanzamt"
		d 1200 161,89
		c 1576 161,89

		2021-09-02
		"Verlust von Computer 1"
		d 2320 1,00
		c 0480 1,00

		2021-09-02
		"Verlust von Computer 1. Korrektur der automatischen Restwert-Abschreibung"
		d 0480 1,00
		c 4855 1,00

		2021-09-07
		"Computer 2"
		d 0480 738,66
		d 1576 140,34
		c 1200 879,00

		2021-10-06
		"Umsatzsteuererstattung vom Finanzamt"
		d 1200 140,34
		c 1576 140,34

		2021-11-06
		"IHK Mitgliedsbeiträge 2019 + 2021"
		d 4980 128,00
		c 1200 128,00
		"""

	static let archivedLedgerWithIssues1 =
		"""
		[format]
		1

		[period]
		2021-01-01 - 2021-12-31

		[company]
		name My Small Company
		seat street Ballindamm
		seat houseno 2
		seat zipcode 20095
		seat city Hamburg
		seat country Deutschland
		regc Hamburg-Mitte
		regn HRB 111111
		scap 7.000,00
		ceo- Hansi Kleinmann; Ballindamm 2, 20095 Hamburg
		st13 1234567890123
		bf4- 1234
		size tiny
		does Einzelhandel
		empl 3
		liqu no

		[assets]
		= 1
		equipment
		"Drucker"
		acqd 2019-11-13
		cost 108,40
		depr linear 3
		= 2
		machinery
		"Fahrrad"
		acqd 2020-03-18
		cost 461,34
		depr gwg
		= 3
		equipment
		"Computer 1"
		acqd 2021-05-24
		retd 2021-09-02
		cost 738,66
		depr gwg
		= 4
		equipment
		"Computer 2"
		acqd 2021-09-07
		cost 738,66
		depr gwg
		+
		cash
		"Bank"
		valu 3.618,73
		+
		receivable
		"derived"
		valu 1.000,00
		+
		receivable
		"derived"
		valu 500,00

		[journal]
		chart SKR03

		2021-01-04
		"Computerzubehör"
		d 4980 150,39
		d 1576 24,06
		c 1200 174,45

		2021-01-08
		"Umsatzsteuererstattung vom Finanzamt"
		d 1200 25,35
		c 1576 25,35

		2021-01-27
		"Datenspeicher"
		d 4980 100,67
		d 1576 19,13
		c 1200 119,80

		2021-03-08
		"Lizenzgebuehr fuer Bildmarke"
		d 4964 60,00
		c 1200 60,00

		2021-03-08
		"Geschäftsführergehalt für 2020"
		d 4124 1,00
		c 1200 1,00

		2021-03-08
		"Gebühr für Mitbenutzung von Internet-Domain und Webhosting"
		d 4925 36,00
		c 1200 36,00

		2021-03-08
		"Miete/Raumkosten 2020"
		d 4200 36,00
		c 1200 36,00

		2021-03-08
		"Miete/Raumkosten 2020"
		d 4200 36,00
		c 1200 36,00

		2021-04-09
		"Umsatzsteuererstattung vom Finanzamt"
		d 1200 19,13
		c 1576 19.13

		2021-05-24
		"Computer 1"
		tags Project GrandSlam
		d 0480 738,66
		d 1576 140,34
		c 1200 879,00

		2021-06-28
		"Computer 1 Zubehör"
		d 4980 113,45
		d 1576 21,55
		c 1200 135,00

		2021-07-14
		"Umsatzsteuererstattung vom Finanzamt"
		d 1200 161,89
		c 1576 161,89

		2021-09-02
		"Verlust von Computer 1"
		tags Project GrandSlam
		d 2320 1,00
		c 0480 1,00

		2021-09-07
		"Computer X"
		d 0480 738,66
		d 1576 140,34
		c 1200 879,00

		2021-10-06
		"Umsatzsteuererstattung vom Finanzamt"
		d 1200 140,34
		c 1576 140,34

		2021-11-06
		"IHK Mitgliedsbeiträge 2019 + 2021"
		d 4980 128,00
		c 1200 128,00
		"""

	// MARK: -

	static let report1 : Report = {
		let period = Period.year(2020)!
		var inventory = inventory1
		inventory.assets = nil
		inventory.fixedAssetDepreciationSinceLastPeriod = nil
		return Report(title:ReportTitle("Jahresabschluss 2020"),
		type: .trade,
		taxonomy: DEGAAPTaxonomyVersion(date: period.end.midday) ?? .allCases.last!,
		period: period,
		inventory: inventory,
		incomeStatement: incomeStatement2,
		balanceSheet: balanceSheet2,
		notesBelowBalanceSheet: notesBelowBalanceSheet1,
		notes: notes1,
		accountTotals: accountTotals1)
	}()

	static let archivedTradeReport1 =
		"""
		[format]
		1

		[title]
		Jahresabschluss 2020

		[type]
		Handelsbilanz

		[period]
		2020-01-01 - 2020-12-31

		[taxonomy]
		6.3

		[inventory]
		\(archivedInventory1)

		[income_statement]
		\(archivedIncomeStatement2)

		[balance_sheet]
		\(archivedBalanceSheet2)

		[balance_sheet_notes]
		\(archivedNotesBelowBalanceSheet1)

		[notes.asset_changes]
		\(archivedAssetChangesStatement1)

		[account_totals]
		\(archivedAccountTotals1)
		"""

	static let archivedTradeReport3 =
		"""
		[format]
		1

		[title]
		Jahresabschluss 2019

		[type]
		Handelsbilanz

		[period]
		2019-10-28 - 2019-12-31

		[taxonomy]
		6.3

		[inventory]
		2019-12-31
		fixd equipment 102,38
		curr receivable 44,88
		curr cash 6.684,07
		debt short 150,00

		[income_statement]
		earnings 0,00
		earnings_other 0,00
		material 0,00
		salaries 0,00
		depreciations 6,02
		expenses 312,65
		tax 0,00

		[balance_sheet]
		asset fixed 102,38
		asset current 6.728,95
		asset deferrals 0,00
		asset deferred_tax 0,00
		asset surplus 0,00
		eqlia equity 6.681,33
		eqlia provisions 0,00
		eqlia liabilities 150,00
		eqlia deferrals 0,00
		eqlia deferred_tax 0,00

		[balance_sheet_notes]
		name My Small Company
		seat Hamburg
		regc Hamburg-Mitte
		regn HRB 111111
		scap 7.000,00
		ceo- Hansi Kleinmann; Ballindamm 2, 20095 Hamburg
		size tiny
		empl 3
		liqu no
		meth GKV

		[account_totals]
		bs.ass.SurplusFromOffsetting 0,00
		bs.ass.currAss 6.728,95
		bs.ass.defTax 0,00
		bs.ass.fixAss 102,38
		bs.ass.prepaidExp 0,00
		bs.eqLiab.accruals 0,00
		bs.eqLiab.defIncome 0,00
		bs.eqLiab.defTax 0,00
		bs.eqLiab.equity.retainedEarnings.finalPrev 318,67
		bs.eqLiab.equity.subscribed.corp -7.000,00
		bs.eqLiab.liab -150,00
		"""

	static let archivedFiscalReport3 =
		"""
		[format]
		1

		[title]
		Jahresabschluss 2019

		[type]
		Steuerbilanz

		[period]
		2019-10-28 - 2019-12-31

		[taxonomy]
		6.3

		[inventory]
		2019-12-31
		fixd equipment_office 102,38
		curr receivable 44,88
		curr cash 6.684,07
		debt short 150,00

		[income_statement]
		earnings 0,00
		earnings_other 0,00
		material 0,00
		salaries 0,00
		depreciations 6,02
		expenses 312,65
		tax 0,00

		[balance_sheet]
		asset fixed 102,38
		asset current 6.728,95
		asset deferrals 0,00
		asset deferred_tax 0,00
		asset surplus 0,00
		eqlia equity 6.681,33
		eqlia provisions 0,00
		eqlia liabilities 150,00
		eqlia deferrals 0,00
		eqlia deferred_tax 0,00

		[balance_sheet_notes]
		name My Small Company
		seat street Ballindamm
		seat houseno 2
		seat zipcode 20095
		seat city Hamburg
		seat country Deutschland
		form GmbH
		does Einzelhandel
		regc Hamburg-Mitte
		regn HRB 111111
		scap 7.000,00
		st13 1234567890123
		bf4- 1234
		ceo- Hansi Kleinmann; Ballindamm 2, 20095 Hamburg
		size tiny
		empl 3
		liqu no
		meth GKV

		[notes.asset_changes]
		>>> bs.ass.fixAss.tan.otherEquipm.office
		acq begin 0,00
		acq new 108,40
		acq interestPaid 0,00
		acq retirements 0,00
		acq rebookings 0,00
		acq end 108,40
		dep begin 0,00
		dep new 6,02
		dep retirements 0,00
		dep end 6,02
		bkv current 102,38
		bkv previous 0,00
		<<<

		[account_totals]
		bs.ass.currAss.cashEquiv.bank 6.684,07
		bs.ass.currAss.receiv.other.vat 44,88
		bs.ass.fixAss.tan.otherEquipm.office 102,38
		bs.eqLiab.equity.netIncome 318,67
		bs.eqLiab.equity.subscribed.corp -7.000,00
		bs.eqLiab.liab.other.shareholders -150,00
		is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.otherMisc 6,02
		is.netIncome.regular.operatingTC.otherCost.legalConsulting 127,75
		is.netIncome.regular.operatingTC.otherCost.miscellaneous.misc 184,90

		[bvv]
		bs.ass.deficitNotCoveredByCapital 0,00
		BVV.profitLoss.assetsCurrentYear -6.681,33
		BVV.profitLoss.assetsPreviousYear.assets 0,00
		BVV.profitLoss.contribution 0,00
		BVV.profitLoss.withdrawalDistrib 0,00
		"""

	static let archivedFiscalReport4 =
		"""
		[format]
		1

		[title]
		Jahresabschluss 2020

		[type]
		Steuerbilanz

		[period]
		2020-01-01 - 2020-12-31

		[previous_period]
		2019-10-28 - 2019-12-31

		[taxonomy]
		6.4

		[inventory]
		2020-12-31
		fixd equipment_office 67,26
		curr receivable 1,29
		curr cash 5.720,27

		[income_statement]
		earnings 0,00
		earnings_other 0,00
		material 0,00
		salaries 1,00
		depreciations 496,46 6,02
		expenses 395,05 312,65
		tax 0,00

		[balance_sheet]
		asset fixed 67,26 102,38
		asset current 5.721,56 6.728,95
		asset deferrals 0,00
		asset deferred_tax 0,00
		asset surplus 0,00
		eqlia equity 5.788,82 6.681,33
		eqlia provisions 0,00
		eqlia liabilities 0,00 150,00
		eqlia deferrals 0,00
		eqlia deferred_tax 0,00

		[balance_sheet_notes]
		name My Small Company
		seat street Ballindamm
		seat houseno 2
		seat zipcode 20095
		seat city Hamburg
		seat country Deutschland
		regc Hamburg-Mitte
		regn HRB 111111
		scap 7.000,00
		ceo- Hansi Kleinmann; Ballindamm 2, 20095 Hamburg
		st13 1234567890123
		bf4- 1234
		size tiny
		does Einzelhandel
		empl 3
		liqu no
		meth GKV

		[notes.asset_changes]
		>>> bs.ass.fixAss.tan.otherEquipm.gwg
		acq begin 0,00
		acq new 461,34
		acq interestPaid 0,00
		acq retirements 0,00
		acq rebookings 0,00
		acq end 461,34
		dep begin 0,00
		dep new 460,34
		dep retirements 0,00
		dep end 460,34
		bkv current 1,00
		bkv previous 0,00
		<<<
		>>> bs.ass.fixAss.tan.otherEquipm.office
		acq begin 108,40
		acq new 0,00
		acq interestPaid 0,00
		acq retirements 0,00
		acq rebookings 0,00
		acq end 108,40
		dep begin 6,02
		dep new 36,12
		dep retirements 0,00
		dep end 42,14
		bkv current 66,26
		bkv previous 102,38
		<<<

		[account_totals]
		bs.ass.currAss.cashEquiv.bank 5.720,27
		bs.ass.currAss.receiv.other.vat 1,29
		bs.ass.fixAss.tan.otherEquipm.gwg 1,00
		bs.ass.fixAss.tan.otherEquipm.office 66,26
		bs.eqLiab.accruals 0,00
		bs.eqLiab.defIncome 0,00
		bs.eqLiab.defTax 0,00
		bs.eqLiab.equity.retainedEarnings.finalPrev 1.211,18
		bs.eqLiab.equity.subscribed.corp -7.000,00
		bs.eqLiab.liab 0,00

		[bvv]
		bs.ass.deficitNotCoveredByCapital 0,00
		BVV.profitLoss.assetsCurrentYear -5.788,82
		BVV.profitLoss.assetsPreviousYear.assets -6.831,33
		BVV.profitLoss.withdrawalDistrib 0,00
		BVV.profitLoss.contribution 0,00
		"""

	static let archivedFiscalReport5 =
		"""
		[format]
		1

		[title]
		Jahresabschluss 2021

		[type]
		Steuerbilanz

		[period]
		2021-01-01 - 2021-12-31

		[previous_period]
		2020-01-01 - 2020-12-31

		[taxonomy]
		6.4

		[inventory]
		2021-12-31
		fixd equipment 1,00
		fixd equipment_office 31,14
		curr cash 3.618,73

		[income_statement]
		earnings 0,00
		earnings_other 0,00
		material 0,00
		salaries 1,00 1,00
		depreciations 1.511,44 496,46
		expenses 625,51 395,05
		tax 0,00

		[balance_sheet]
		asset fixed 32,14 67,26
		asset current 3.618,73 5.721,56
		asset deferrals 0,00
		asset deferred_tax 0,00
		asset surplus 0,00
		eqlia equity 3.650,87 5.788,82
		eqlia provisions 0,00
		eqlia liabilities 0,00
		eqlia deferrals 0,00
		eqlia deferred_tax 0,00

		[balance_sheet_notes]
		name My Small Company
		seat street Ballindamm
		seat houseno 2
		seat zipcode 20095
		seat city Hamburg
		seat country Deutschland
		regc Hamburg-Mitte
		regn HRB 111111
		scap 7.000,00
		ceo- Hansi Kleinmann; Ballindamm 2, 20095 Hamburg
		st13 1234567890123
		bf4- 1234
		size tiny
		does Einzelhandel
		empl 3
		liqu no
		meth GKV

		[notes.asset_changes]
		>>> bs.ass.fixAss.tan.otherEquipm.gwg
		acq begin 461,34
		acq new 1.477,32
		acq interestPaid 0,00
		acq retirements 738,66
		acq rebookings 0,00
		acq end 1.200,00
		dep begin 460,34
		dep new 1.475,32
		dep retirements 737,66
		dep end 1.198,00
		bkv current 2,00
		bkv previous 1,00
		<<<
		>>> bs.ass.fixAss.tan.otherEquipm.office
		acq begin 108,40
		acq new 0,00
		acq interestPaid 0,00
		acq retirements 0,00
		acq rebookings 0,00
		acq end 108,40
		dep begin 42,14
		dep new 36,12
		dep retirements 0,00
		dep end 78,26
		bkv current 30,14
		bkv previous 66,26
		<<<

		[account_totals]
		bs.ass.currAss.cashEquiv.bank 3.618,73
		bs.ass.currAss.receiv.other.vat 0,00
		bs.ass.fixAss.tan.otherEquipm.gwg 2,00
		bs.ass.fixAss.tan.otherEquipm.office 30,14
		bs.eqLiab.accruals.other.other 0,00
		bs.eqLiab.defIncome 0,00
		bs.eqLiab.defTax 0,00
		bs.eqLiab.equity.retainedEarnings.finalPrev 3.349,13
		bs.eqLiab.equity.subscribed.corp -7.000,00
		bs.eqLiab.liab.other.other 0,00
		bs.eqLiab.equity.netIncome 2.137,95
		bs.eqLiab.equity.profitLoss.retainedEarnings 0
		bs.eqLiab.equity.retainedEarnings.finalPrev 1.211,18
		is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.lowValueAs 1.475,32
		is.netIncome.regular.operatingTC.deprAmort.fixAss.tan.otherMisc 36,12
		is.netIncome.regular.operatingTC.otherCost.communication 36
		is.netIncome.regular.operatingTC.otherCost.concessLicenses 60
		is.netIncome.regular.operatingTC.otherCost.disposFixAss.misc 1
		is.netIncome.regular.operatingTC.otherCost.leaseFix.shareholders 36
		is.netIncome.regular.operatingTC.otherCost.miscellaneous.misc 492,51
		is.netIncome.regular.operatingTC.staff.salaries.managerPartner 1

		[bvv]
		bs.ass.deficitNotCoveredByCapital 0,00
		BVV.profitLoss.assetsCurrentYear -3.650,87
		BVV.profitLoss.assetsPreviousYear.assets -5.788,82
		BVV.profitLoss.contribution 0,00
		BVV.profitLoss.withdrawalDistrib 0,00
		"""
}
