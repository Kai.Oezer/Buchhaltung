// Copyright 2022-2025 Kai Oezer

import Testing
@testable import Buchhaltung

@Suite
struct MoneyTests
{
	@Test
	func initialization() throws
	{
		let (a, b) = Money(10, 5).components
		#expect(a == 10)
		#expect(b == 5)
		#expect(Money(13.1) == Money(13, 10))
		#expect(Money(13.10) == Money(13, 10))
		#expect(Money(13.109) == Money(13, 10))
		#expect(Money(89_344_911, 09) == 89_344_911.09)
	}

	@Test
	func arithmetics()
	{
		#expect(Money(3,43) + Money(27,07) == Money(30,50))
		#expect(Money(4,15) - Money(1,15) == Money(3,0))
		#expect(Money(15)/Money(2) == 7.5) // accuracy: 1.0 / Double(Money.subunitFactor) / 10
		var m1 = Money(12)
		m1 /= 3
		#expect(m1 == Money(4))
		let m2 = m1.negated
		#expect(m2 == Money(-4))
		#expect(m2.absolute == Money(4))
		m1 -= 1.5
		#expect(m1 == Money(2.5))
		m1 += 4.5
		#expect(m1 == 7)
		#expect(m1 / UInt(2) == 3.5)
		#expect(m1 * 5 == Money(35))
		#expect(5 * m1 == Money(35))
	}

	@Test
	func comparing()
	{
		#expect(Money(3,4) < Money(4,5))
		#expect((Money(5,1) < Money(3,4)) == false)
		#expect(Money(3,4) <= 3.4)
		#expect((Money(4,3) <= 3.4) == false)
		#expect(Money(3,4) > 3.0)
		#expect((Money(3,4) > 5.0) == false)
		#expect(Money(3,4) >= 3.0)
		#expect((Money(3,4) >= 5.0) == false)
	}

	@Test("string representation", arguments: [
		(4, true, true, "4,00"),
		(Money(4,3), true, true, "4,03"),
		(Money(4,13), true, true, "4,13"),
		(Money(1_050,13), true, true, "1.050,13"),
		(Money(-475,13), true, true, "-475,13"),
		(Money(-12_340,55), true, true, "-12.340,55"),
		(Money(345_666_239,9), true, true, "345.666.239,09"),
		(Money(-12_340,55), false, true, "-12,340.55"),
		(Money(-12_340,55), false, false, "-12340.55")
	])
	func stringRepresentation(value : Money, usingComma : Bool, separator : Bool, expectedString : String) throws
	{
		#expect(value.decimalStringRepresentation(usingCommaAsDecimalSeparator: usingComma, withThousandsSeparator: separator) == expectedString)
	}

	@Test("BH formatting", .tags(.bhCoding), arguments: [
		(Money(5), "5,00"),
		(Money(49, 12), "49,12"),
		(Money(4911,12), "4.911,12")
	])
	func bhFormatting(value : Money, expectedString : String) throws
	{
		#expect(value.bh == expectedString)
	}

	@Test("BH parsing", .tags(.bhCoding))
	func bhParsing() throws
	{
		#expect(throws: (any Error).self) { try Money("a", strategy: .bhMoney) }
		#expect(try Money("5", strategy: .bhMoney) == Money(5))
		#expect(try Money("5 €", strategy: .bhMoney) == Money(5))
		#expect(try Money("5 bla blabla ", strategy: .bhMoney) == Money(5))
		#expect(try Money("5,00", strategy: .bhMoney) == Money(5))
		#expect(try Money("2,1", strategy: .bhMoney) == Money(2,10))
		#expect(try Money("2,01", strategy: .bhMoney) == Money(2,1))
		#expect(try Money("2,001", strategy: .bhMoney) == Money(2))
		#expect(try Money("19,90", strategy: .bhMoney) == Money(19,90))
		withKnownIssue {
			#expect(throws: (any Error).self) { try Money("-5,0-0", strategy: .bhMoney) }
		} when: {
			#if os(Linux)
				true
			#else
				false
			#endif
		}
		#expect(try Money("-5", strategy: .bhMoney) == Money(-5))
		#expect(try Money("-475,13", strategy: .bhMoney) == Money(-475,13))
		#expect(try Money("1234567,18", strategy: .bhMoney) == Money(1234567,18))
		#expect(try Money("1.234.567,18", strategy: .bhMoney) == Money(1234567,18))
		#expect(try Money("1.2.3.4...56.7,1.8", strategy: .bhMoney) == Money(1234567,18))
		#expect(throws: (any Error).self) { try Money("3.70", strategy: .bhMoney) }
	}

}
