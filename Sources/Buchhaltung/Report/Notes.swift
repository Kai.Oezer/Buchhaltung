// Copyright 2021-2024 Kai Oezer

import Foundation

/**
 German: Anhang

 Die gesetzlichen Vertreter einer Kapitalgesellschaft haben den Jahresabschluß
 um einen Anhang zu erweitern, der mit der Bilanz und der Gewinn- und Verlustrechnung
 eine Einheit bildet, sowie einen Lagebericht aufzustellen
 [(§ 264 Abs. 1 HGB)](https://www.gesetze-im-internet.de/hgb/__264.html).

 __Kleinstkapitalgesellschaften__ ([§ 267a HGB](https://www.gesetze-im-internet.de/hgb/__267a.html))
 sind jedoch davon ausgenommen ([§ 274a HGB](https://www.gesetze-im-internet.de/hgb/__274a.html))
 wenn unter der Bilanz folgende Angaben gemacht werden. Siehe ``NotesBelowBalanceSheet``.
*/
public struct Notes : Equatable, Sendable
{
	public var assetChangesStatement : AssetChangesStatement?
}

extension Notes : Codable
{
	enum CodingKeys : String, CodingKey
	{
		case assetChangesStatement = "asset_changes"
	}
}
