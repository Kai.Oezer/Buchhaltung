//  Copyright 2020-2024 Kai Oezer

import Foundation

/**
German: Bilanz

[§ 266 HGB](https://www.gesetze-im-internet.de/hgb/__266.html)

Die Bilanz ist in **Kontoform** aufzustellen. Darin werden Aktiva und Passiva in einer "T"-förmigen,
zweispaltigen Ansicht in einer Gegenüberstellung angezeigt; Aktiva links, Passiva rechts.
* Die Summe der Beträge auf der linken Seite ist gleich der Summe der Beträge auf der rechten Seite.
* Der Saldo (Summe der Beträge) wird auf der Seite angezeigt die weniger Einträge hat.
* Aktiva und Passiva zeigen jeweils in einer Unterspalte die Vorjahreswerte der Bilanzposten an.

Die Klause über die Kontoform ist natürlich irrelevant für der Übermittlung als e-Bilanz in de-gaap Format.

---

[§ 265 HGB](https://www.gesetze-im-internet.de/hgb/__265.html)

In der Bilanz sowie in der Gewinn- und Verlustrechnung ist zu jedem Posten der entsprechende
Betrag des vorhergehenden Geschäftsjahrs anzugeben. Sind die Beträge nicht vergleichbar,
so ist dies im Anhang anzugeben und zu erläutern.

---

Die **verkürzte Gliederung der Bilanz für Kleinstkapitalgesellschaften** (in dieser Reihenfolge):

Aktiva
* Anlagevermögen
* Umlaufvermögen
* Rechnungsabgrenzungsposten
* Aktive latente Steuern
* Aktiver Unterschiedsbetrag aus der Vermögensverrechnung

Passiva
* Eigenkapital
* Rückstellungen
* Verbindlichkeiten
* Rechnungsabgrenzungsposten
* Passive latente Steuern

---

**Bilanzvermerke** ( [§ 268 HGB](https://www.gesetze-im-internet.de/hgb/__268.html) )
1. Die Bilanz darf auch unter Berücksichtigung der vollständigen oder teilweisen Verwendung des Jahresergebnisses aufgestellt werden.
Wird die Bilanz unter Berücksichtigung der **teilweisen Verwendung des Jahresergebnisses** aufgestellt, so tritt an die Stelle der Posten
"Jahresüberschuß/Jahresfehlbetrag" und "Gewinnvortrag/Verlustvortrag" der Posten **"Bilanzgewinn/Bilanzverlust"**; ein vorhandener
Gewinn- oder Verlustvortrag ist in den Posten "Bilanzgewinn/Bilanzverlust" einzubeziehen und in der Bilanz gesondert anzugeben.
Die Angabe kann auch im Anhang gemacht werden.
2. (weggefallen)
3. Ist das Eigenkapital durch Verluste aufgebraucht und ergibt sich ein Überschuß der Passivposten über die Aktivposten, so ist dieser Betrag
am Schluß der Bilanz auf der Aktivseite gesondert unter der Bezeichnung "Nicht durch Eigenkapital gedeckter Fehlbetrag" auszuweisen.
4. Der Betrag der Forderungen mit einer Restlaufzeit von mehr als einem Jahr ist bei jedem gesondert ausgewiesenen Posten zu vermerken.
Werden unter dem Posten "sonstige Vermögensgegenstände" Beträge für Vermögensgegenstände ausgewiesen, die erst nach dem
Abschlußstichtag rechtlich entstehen, so müssen Beträge, die einen größeren Umfang haben, im Anhang erläutert werden.
__Keine Erläuterungspflicht für Kleine Kapitalgesellschaften__ ( siehe [§ 274a HGB](https://www.gesetze-im-internet.de/hgb/__274a.html) ).
5. Der Betrag der Verbindlichkeiten mit einer Restlaufzeit bis zu einem Jahr und der Betrag der Verbindlichkeiten mit
einer Restlaufzeit von mehr als einem Jahr sind bei jedem gesondert ausgewiesenen Posten zu vermerken.
Erhaltene Anzahlungen auf Bestellungen sind, soweit Anzahlungen auf Vorräte nicht von dem Posten "Vorräte" offen
abgesetzt werden, unter den Verbindlichkeiten gesondert auszuweisen. Sind unter dem Posten "Verbindlichkeiten"
Beträge für Verbindlichkeiten ausgewiesen, die erst nach dem Abschlußstichtag rechtlich entstehen, so müssen
Beträge, die einen größeren Umfang haben, im Anhang erläutert werden.
__Keine Erläuterungspflicht für Kleine Kapitalgesellschaften__ ( siehe [§ 274a HGB](https://www.gesetze-im-internet.de/hgb/__274a.html) ).
6. Ein nach [§ 250 Abs. 3](https://www.gesetze-im-internet.de/hgb/__250.html) in den
Rechnungsabgrenzungsposten auf der Aktivseite aufgenommener Unterschiedsbetrag ist in der Bilanz
gesondert auszuweisen oder im Anhang anzugeben.
__Keine Pflicht für Kleine Kapitalgesellschaften__ ( siehe [§ 274a HGB](https://www.gesetze-im-internet.de/hgb/__274a.html) ).
7. Für die in [§ 251](https://www.gesetze-im-internet.de/hgb/__250.html) bezeichneten Haftungsverhältnisse sind
	1. die Angaben zu nicht auf der Passivseite auszuweisenden Verbindlichkeiten und Haftungsverhältnissen im Anhang zu machen,
	2. dabei die Haftungsverhältnisse jeweils gesondert unter Angabe der gewährten Pfandrechte und sonstigen Sicherheiten anzugeben und
	3. dabei Verpflichtungen betreffend die Altersversorgung und Verpflichtungen gegenüber verbundenen oder assoziierten Unternehmen jeweils gesondert zu vermerken.
8. Werden **selbst geschaffene immaterielle Vermögensgegenstände** des Anlagevermögens in der Bilanz ausgewiesen,
so dürfen Gewinne nur ausgeschüttet werden, wenn die nach der Ausschüttung verbleibenden frei verfügbaren Rücklagen zuzüglich
eines Gewinnvortrags und abzüglich eines Verlustvortrags mindestens den insgesamt angesetzten Beträgen abzüglich der hierfür
gebildeten passiven latenten Steuern entsprechen. Werden aktive latente Steuern in der Bilanz ausgewiesen, ist Satz 1 auf den Betrag
anzuwenden, um den die aktiven latenten Steuern die passiven latenten Steuern übersteigen. Bei Vermögensgegenständen im Sinn des
[§ 246 Abs. 2 Satz 2](https://www.gesetze-im-internet.de/hgb/__246.html) ist Satz 1 auf den Betrag abzüglich der
hierfür gebildeten passiven latenten Steuern anzuwenden, der die Anschaffungskosten übersteigt.
*/
public struct BalanceSheet : Codable, Equatable, Sendable
{
	/// German: Aktiva
	public struct Assets : Codable, Equatable, Sendable
	{
		/// Long-term assets.
		/// Increases the sum.
		/// German: Anlagevermögen
		public var fixedAssets : Money = .zero

		/// Short-term assets
		/// Increases the sum.
		/// German: Umlaufvermögen
		public var currentAssets : Money = .zero

		/// Income or expenses that have not realized in the reported period.
		/// Increases or decreases the sum.
		/// German: Rechnungsabgrenzungsposten
		public var deferrals = Deferrals()

		/// Receivable tax returns resulting from the difference in taxation rules for trade and fiscal business reports.
		/// Increases the sum.
		/// German: Aktive latente Steuern
		public var deferredTax = DeferredTax()

		/// Difference due to the company's valuation being higher than the sum of the book values of its assets.
		/// Increases the sum.
		/// German: Aktiver Unterschiedsbetrag aus der Vermögensverrechnung
		public var surplus = SurplusFromOffsetting()

		/// German: Bilanzsumme
		public var sum : Money {
			fixedAssets
			+ currentAssets
			+ deferrals
			+ deferredTax
			+ surplus
		}
	}

	public var assets : Assets
	public var previousPeriodAssets : Assets

	/// Equity and Liabilities are the source of the capitalization.
	/// German: Passiva
	public struct EquityAndLiabilities : Codable, Equatable, Sendable
	{
		/// The money owed to shareholders.
		/// Increases the sum.
		/// German: Eigenkapital
		public var equity : Money = .zero

		/// Money that has been put aside to pay for liabilities in the future.
		/// Decreases the sum.
		/// German: Rückstellungen
		public var provisions : Money = .zero

		/// Money owed to third parties.
		/// Increases the sum
		/// German: Verbindlichkeiten
		public var liabilities : Money = .zero

		/// Expenses or income deferred to next period
		/// Can increase or decrease the sum.
		/// German: Rechnungsabgrenzungsposten
		public var deferrals = Deferrals()

		/// Owed tax resulting from the difference in taxation rules for trade and fiscal business reports.
		/// Increases the sum.
		/// German: Passive latente Steuern
		public var deferredTax = DeferredTax()

		/// German: Bilanzsumme
		public var sum : Money {
			equity
			- provisions
			+ liabilities
			+ deferrals
			+ deferredTax
		}
	}

	public var eqLiab : EquityAndLiabilities
	public var previousPeriodEqLiab : EquityAndLiabilities

	init()
	{
		assets = Assets()
		previousPeriodAssets = Assets()

		eqLiab = EquityAndLiabilities()
		previousPeriodEqLiab = EquityAndLiabilities()
	}

	public var isBalanced : Bool
	{
		assets.sum == eqLiab.sum
	}
}

/**
Accruals and deferrals (to next business period)

German: Rechnungsabgrenzungsposten

Wenn Zahlungen in einer Periode geleistet werden, die zugehörigen Aufwände oder Erträge
aber in einer anderen Periode anfallen, muss eine Rechungsabgrenzung vorgenommen werden.
Ein Beispiel für Rechnungsabgrenzung ist die Voranzahlung für eine Sache, die erst in der nächsten
Bilanzperiode eintritt. In diesem Fall wird ein _aktiver Rechnungsabgrenzungsposten_ (**aRAP**)
gebildet, auf der Seite der Aktiva der Bilanz,  der das Recht auf die Sache in der nächsten Periode reflektiert.
*/
public struct Deferrals : Codable, MoneyEquivalent, Equatable, Sendable
{
	public var monetaryValue : Money = .zero
}

/**
Deferred tax

- Tag: bs-latente-steuern

German: Latente Steuern

Latente Steuern dienen dazu, eine mögliche Differenz zwischen der Steuerschuld aus
Steuerbilanz und Handelsbilanz zu erfassen.
[§ 274 HGB](https://www.gesetze-im-internet.de/hgb/__274.html)

_Aktive latente Steuern_ entstehen wenn der Steueraufwand in der Handelsbilanz
(fiktive Steuern) geringer ist als der Steueraufwand in der Steuerbilanz (effektive Steuern).
Aktive latente Steuern sind wie eine Forderung gegenüber dem Finanzamt zu verstehen.

_Passive latente Steuern_ entstehen wenn der Steueraufwand in der Handelsbilanz
größer ausfällt als in der Steuerbilanz, zum Beispiel, durch immaterielle Anlagegüter.
Die Entwicklungskosten von selbst erstellter Software können in der Handelsbilanz
als selbsterstellte immaterielle Anlagegüter ausgewiesen werden anstatt als reine
Aufwendung. Das bedeutet, dass die Herstellungskosten für diese immaterielle Anlage
nicht auf die zu zahlende Steuer angerechnet werden kann, somit die Steuerlast erhöht.
Forschungskosten bleiben jedoch immer Aufwendung und sind nicht in immaterielle
Anlagegüter konvertierbar
( [§ 255 Abs. 2a HGB](https://www.gesetze-im-internet.de/hgb/__255.html) ).

**Kleine Kapitalgesellschaften sind von der Bilanzierung latenter Steuern befreit.**
Siehe [§ 274a Nr. 4 HGB](https://www.gesetze-im-internet.de/hgb/__274a.html)
*/
public struct DeferredTax : Codable, MoneyEquivalent, Equatable, Sendable
{
	public var monetaryValue : Money = .zero
}

/**
Excess value of assets that are reserved for fulfilling pension liabilities.

German: Unterschiedsbetrag aus der Vermögensverrechnung

Erfasst den Unterschied zwischen Vermögensgegenständen und Schulden
die ausschließlich zur Erfüllung von Schulden aus Altersversorgungsverpflichtungen
dienen. Diese Position ist bei einer GmbH nur dann relevant, wenn den Geschäftsführern
Pensionszusagen erteilt wurden.
[§ 246 Abs. 2 HGB](https://www.gesetze-im-internet.de/hgb/__246.html)
*/
public struct SurplusFromOffsetting : Codable, MoneyEquivalent, Equatable, Sendable
{
	public var monetaryValue : Money = .zero
}
