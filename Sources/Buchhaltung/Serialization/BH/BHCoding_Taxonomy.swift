// Copyright 2023-2024 Kai Oezer

import DEGAAP
import TOMLike
import IssueCollection

extension DEGAAPTaxonomyVersion : @retroactive TOMLikeEmbeddable
{
	public func tomlikeEmbedded() throws -> String
	{
		self.humanReadable
	}

	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		guard lines.count == 1, let input = lines.first,
			let decodedTaxonomy = DEGAAPTaxonomyVersion(fromHumanReadable: input.content)
			else { throw BHCodingError.invalidArchiveFormat }
		self = decodedTaxonomy
	}
}
