//  Copyright 2021-2022 Kai Oezer

import Foundation
import IssueCollection
import TOMLike

private let id_earnings = "earnings"
private let id_otherEarnings = "earnings_other"
private let id_materialCosts = "material"
private let id_salaries = "salaries"
private let id_depreciations = "depreciations"
private let id_otherExpenses = "expenses"
private let id_tax = "tax"

extension IncomeStatement : TOMLikeEmbeddable
{
	public func tomlikeEmbedded() throws -> String
	{
		var lines = [String]()

		let appendLine : (String, KeyPath<IncomeStatement.Results, Money>) -> Void = { identifier, keypath in
			var entry = "\(identifier) \(results[keyPath: keypath].bh)"
			let previousYearValue = previousYearResults[keyPath: keypath]
			if previousYearValue > .zero
			{
				entry += " \(previousYearValue.bh)"
			}
			lines.append(entry)
		}

		appendLine(id_earnings, \.earnings)
		appendLine(id_otherEarnings, \.otherEarnings)
		appendLine(id_materialCosts, \.materialCosts)
		appendLine(id_salaries, \.salaries)
		appendLine(id_depreciations, \.depreciations)
		appendLine(id_otherExpenses, \.otherExpenses)
		appendLine(id_tax, \.tax)

		return lines.joined(separator: "\n")
	}

	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		var decodedResults = IncomeStatement.Results()
		var decodedPreviousResults = IncomeStatement.Results()

		for line in lines
		{
			let components = line.content.components(separatedBy: .whitespaces)
			if components.count > 1
			{
				let resultType = components[0]
				let value = (try? Money(components[1], strategy: .bhMoney)) ?? .zero
				let previousValue = components.count > 2 ? ((try? Money(components[2], strategy: .bhMoney)) ?? .zero) : .zero
				if resultType == "earnings" {
					decodedResults.earnings = value
					decodedPreviousResults.earnings = previousValue
				}
				else if resultType == "earnings_other" {
					decodedResults.otherEarnings = value
					decodedPreviousResults.otherEarnings = previousValue
				}
				else if resultType == "material" {
					decodedResults.materialCosts = value
					decodedPreviousResults.materialCosts = previousValue
				}
				else if resultType == "salaries" {
					decodedResults.salaries = value
					decodedPreviousResults.salaries = previousValue
				}
				else if resultType == "depreciations" {
					decodedResults.depreciations = value
					decodedPreviousResults.depreciations = previousValue
				}
				else if resultType == "expenses" {
					decodedResults.otherExpenses = value
					decodedPreviousResults.otherExpenses = previousValue
				}
				else if resultType == "tax" {
					decodedResults.tax = value
					decodedPreviousResults.tax = previousValue
				}
			}
		}

		self.results = decodedResults
		self.previousYearResults = decodedPreviousResults
	}

}
