// Copyright 2020-2023 Kai Oezer
// swiftlint:disable nesting

import Foundation
import IssueCollection
import TOMLike

let bhFixedAssetPrefix = "="
let bhCurrentAssetPrefix = "+"

extension Assets : TOMLikeEmbeddable
{
	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		let decoder = AssetsBHDecoder(lines: lines)
		self = decoder.decode()
		issues.append(decoder.issues)
	}

	public func tomlikeEmbedded() throws -> String
	{
		var archive : [String] = []
		fixedAssets.sorted().forEach {
			archive.append("\(bhFixedAssetPrefix) \($0.registrationID)")
			archive.append("\($0.type)")
			archive.append("\"\($0.name)\"")
			archive.append("acqd \($0.acquisitionDate.bh)")
			if $0.retirementDate < .distantFuture
			{
				archive.append("retd \($0.retirementDate.bh)")
			}
			archive.append("cost \($0.acquisitionCost.bh)")
			if $0.depreciationSchedule.progression == .gwg
			{
				archive.append("depr gwg")
			}
			else if $0.depreciationSchedule.progression == .linear
			{
				archive.append("depr linear \($0.depreciationSchedule.depreciationYears.description)")
			}
		}
		currentAssets.sorted().forEach {
			archive.append(
				"""
				\(bhCurrentAssetPrefix)
				\($0.type)
				\"\($0.name)\"
				valu \($0.value.bh)
				"""
			)
		}
		return archive.isEmpty ? "" : archive.joined(separator: "\n")
	}
}

private extension Assets
{
	class AssetsBHDecoder
	{
		let lines : [IndexedLine]
		var issues = IssueCollection()

		enum ItemDecodingState
		{
			case waitingForItem
			case fixedAsset
			case currentAsset
		}

		enum PropertyDecodingState
		{
			case expectingType
			case expectingTitle
			case expectingAcquisitionDate
			case expectingRetirementDateOrCost
			case expectingCost
			case expectingDepreciation
			case expectingValue
		}

		var itemState : ItemDecodingState = .waitingForItem
		var propertyState : PropertyDecodingState = .expectingType
		var currentFixedAsset : FixedAsset?
		var currentCurrentAsset : CurrentAsset?
		var inventory = Assets()

		init(lines : [IndexedLine])
		{
			self.lines = lines
		}

		func decode() -> Assets
		{
			issues.clear()
			for line in lines
			{
				let trimmedLine = line.content.trimmingCharacters(in: .whitespaces)
				if !_detectItemStart(for: trimmedLine) { _decodeProperty(for: trimmedLine) }
			}
			return inventory
		}

		@discardableResult
		private func _detectItemStart(for line : String) -> Bool
		{
			var nextItemState = ItemDecodingState.waitingForItem

			if line.starts(with: bhFixedAssetPrefix) {
				nextItemState = .fixedAsset
			}
			else if line.starts(with: bhCurrentAssetPrefix) {
				nextItemState = .currentAsset
			}
			if nextItemState != .waitingForItem
			{
				currentFixedAsset = (nextItemState == .fixedAsset) ? FixedAsset(registrationID: _registrationID(from: line)) : nil
				currentCurrentAsset = (nextItemState == .currentAsset) ? CurrentAsset() : nil
				itemState = nextItemState
				propertyState = .expectingType
				return true
			}
			return false
		}

		private func _decodeProperty(for line : String)
		{
			switch itemState
			{
				case .fixedAsset:   _decodeFixedAsset(for: line)
				case .currentAsset: _decodeCurrentAsset(for: line)
				default: return
			}
		}

		// swiftlint:disable cyclomatic_complexity
		private func _decodeFixedAsset(for line : String)
		{
			guard currentFixedAsset != nil else { return }
			switch propertyState
			{
				case .expectingType:
					guard let type = FixedAssetType(from: line) else { itemState = .waitingForItem; return }
					currentFixedAsset!.type = type
					propertyState = .expectingTitle
				case .expectingTitle:
					guard let title = BHCoding.decodeTitle(line) else { itemState = .waitingForItem; return }
					currentFixedAsset!.name = title
					propertyState = .expectingAcquisitionDate
				case .expectingAcquisitionDate:
					guard let date = _date(from: line, prefixedBy: "acqd ") else { itemState = .waitingForItem; return }
					currentFixedAsset!.acquisitionDate = date
					propertyState = .expectingRetirementDateOrCost
				case .expectingRetirementDateOrCost:
					guard let retDate = _date(from: line, prefixedBy: "retd") else { fallthrough }
					currentFixedAsset!.retirementDate = retDate
					propertyState = .expectingCost
				case .expectingCost:
					guard let cost = _money(from: line, prefixedBy: "cost ") else { itemState = .waitingForItem; return }
					currentFixedAsset!.acquisitionCost = cost
					propertyState = .expectingDepreciation
				case .expectingDepreciation:
					guard let (progression, numYears) = _depreciation(from: line, prefixedBy: "depr ") else { itemState = .waitingForItem; return }
					let schedule = DepreciationSchedule(progression: progression, depreciationYears: numYears)
					currentFixedAsset!.depreciationSchedule = schedule
					if currentFixedAsset!.isGWG && !currentFixedAsset!.isEquipment
					{
						issues.append(Issue(domain: .bhCoding, code: .gwgButNotEquipment, message: "Fixed asset named \"\(currentFixedAsset!.name)\" is not of type equipment but uses GWG depreciation."))
						return
					}
					inventory.fixedAssets.insert(currentFixedAsset!)
				default: return
			}
		}

		private func _decodeCurrentAsset(for line : String)
		{
			guard currentCurrentAsset != nil else { return }
			switch propertyState
			{
				case .expectingType:
					guard let type = CurrentAssetType(from: line) else { itemState = .waitingForItem; return }
					currentCurrentAsset!.type = type
					propertyState = .expectingTitle
				case .expectingTitle:
					guard let title = BHCoding.decodeTitle(line) else { itemState = .waitingForItem; return }
					currentCurrentAsset!.name = title
					propertyState = .expectingValue
				case .expectingValue:
					guard let cost = _money(from: line, prefixedBy: "valu ") else { itemState = .waitingForItem; return }
					currentCurrentAsset!.value = cost
					if (currentCurrentAsset!.type == .receivable) && (currentCurrentAsset!.name == "derived")
					{
						issues.append(Issue(domain: .bhCoding, code: .derivedReceivableAsset, message: "The name \"\(currentCurrentAsset!.name)\" is not allowed for current assets of type \(currentCurrentAsset!.type)."))
						return
					}
					inventory.currentAssets.insert(currentCurrentAsset!)
				default: return
			}
		}

		private func _registrationID(from line : String) -> RegistrationID
		{
			String(line[line.index(after:line.startIndex)...]).trimmingCharacters(in: .whitespaces)
		}

		private func _date<T : StringProtocol>(from line : T, prefixedBy prefix : T) -> Day?
		{
			guard line.count >= (prefix.count + 10), line.hasPrefix(prefix) else { return nil }
			let dateString = String(line[line.index(line.startIndex, offsetBy: prefix.count)...])
			return try? Day(dateString, strategy: .bhDay)
		}

		private func _money<T : StringProtocol>(from line : T, prefixedBy prefix : T) -> Money?
		{
			guard line.count > prefix.count, line.hasPrefix(prefix) else { return nil }
			let moneyString = String(line[line.index(line.startIndex, offsetBy: prefix.count)...])
			return try? Money(moneyString, strategy: .bhMoney)
		}

		private func _depreciation<T : StringProtocol>(from line : T, prefixedBy prefix : T) -> (DepreciationSchedule.Progression, UInt)?
		{
			guard line.count > prefix.count, line.hasPrefix(prefix) else { return nil }
			let deprTokens = line[line.index(line.startIndex, offsetBy: prefix.count)...].components(separatedBy: .whitespaces)
			if deprTokens.count == 1,
				 deprTokens[0] == "gwg"
			{
				return (.gwg, 0)
			}
			if deprTokens.count == 2,
				deprTokens[0] == "linear",
				let numYears = UInt(deprTokens[1])
			{
				return (.linear, numYears)
			}
			return nil
		}
	}

}
