// Copyright 2023-2025 Kai Oezer

import Foundation
import DEGAAP

extension DEGAAPSourceChart
{
	public struct BHFormatStyle : ParseableFormatStyle
	{
		public func format(_ value : DEGAAPSourceChart) -> String
		{
			value.rawValue
		}

		public var parseStrategy : BHParseStrategy
		{
			BHParseStrategy()
		}
	}

	public struct BHParseStrategy : ParseStrategy
	{
		public func parse(_ value : String) throws -> DEGAAPSourceChart
		{
			let lcValue = value.lowercased(with: .init(languageCode: .german, languageRegion: .germany))
			if lcValue.starts(with: "ikr") {
				return .ikr
			} else if lcValue.starts(with: "skr03") {
				return .skr03
			} else if lcValue.starts(with: "skr04") {
				return .skr04
			} else {
				throw BHCodingError.invalidArchiveFormat
			}
		}
	}

	public var bh : String
	{
		self.formatted(BHFormatStyle())
	}
}

extension FormatStyle where Self == DEGAAPSourceChart.BHFormatStyle
{
	public static var bhChartSource : DEGAAPSourceChart.BHFormatStyle
	{
		DEGAAPSourceChart.BHFormatStyle()
	}
}

extension ParseStrategy where Self == DEGAAPSourceChart.BHParseStrategy
{
	public static var bhChartSource : DEGAAPSourceChart.BHParseStrategy
	{
		DEGAAPSourceChart.BHParseStrategy()
	}
}
