// Copyright 2021-2022 Kai Oezer

import Foundation
import IssueCollection
import TOMLike

private let prefix_account_begin = ">>>"
private let prefix_account_end = "<<<"
private let prefix_acquisitions = "acq"
private let prefix_depreciations = "dep"
private let prefix_bookvalues = "bkv"

extension AssetChangesStatement : TOMLikeEmbeddable
{
	var acquisitionKeypaths : [(String, WritableKeyPath<AssetChangesStatement.AcquisitionCosts, Money>)] {[
		("begin", \.begin),
		("new", \.new),
		("interestPaid", \.interestPaidForBorrowedCapital),
		("retirements", \.retirements),
		("rebookings", \.rebookings),
		("end", \.end)
	]}

	var depreciationKeypaths : [(String, WritableKeyPath<AssetChangesStatement.Depreciations, Money>)] {[
		("begin", \.begin),
		("new", \.new),
		("retirements", \.retirements),
		("end", \.end)
	]}

	var bookValueKeypaths : [(String, WritableKeyPath<AssetChangesStatement.BookValues, Money>)] {[
		("current", \.current),
		("previous", \.previous)
	]}

	public func tomlikeEmbedded() throws -> String
	{
		var lines = [String]()

		changes.sorted{ $0.key < $1.key }.forEach { accountID, accountChanges in
			lines.append("\(prefix_account_begin) \(accountID)")

			acquisitionKeypaths.forEach {
				lines.append("\(prefix_acquisitions) \($0.0) \(accountChanges.acquisitionCosts[keyPath: $0.1].bh)")
			}

			depreciationKeypaths.forEach {
				lines.append("\(prefix_depreciations) \($0.0) \(accountChanges.depreciations[keyPath: $0.1].bh)")
			}

			bookValueKeypaths.forEach {
				lines.append("\(prefix_bookvalues) \($0.0) \(accountChanges.bookValues[keyPath: $0.1].bh)")
			}

			lines.append(prefix_account_end)
		}

		return lines.joined(separator: "\n")
	}

	// swiftlint:disable cyclomatic_complexity
	public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
	{
		let acquisitionKeypaths = self.acquisitionKeypaths
		let depreciationKeypaths = self.depreciationKeypaths
		let bookValueKeypaths = self.bookValueKeypaths

		var changes = [AccountID : AssetAccountChanges]()
		var currentAccount : AccountID?
		var currentAccountChanges = AssetAccountChanges()

		for line in lines
		{
			let components = line.content.components(separatedBy: .whitespaces)
			if components.count == 1 && components[0] == prefix_account_end && currentAccount != nil
			{
				changes[currentAccount!] = currentAccountChanges
			}
			else if components.count == 2 && components[0] == prefix_account_begin
			{
				currentAccount = AccountID(components[1])
			}
			else if components.count > 2
			{
				guard let value = try? Money(components[2], strategy: .bhMoney) else { return }
				let identifier = components[1]
				if components[0] == prefix_acquisitions
				{
					if let valueKeyPath = acquisitionKeypaths.first(where:{$0.0 == identifier})?.1 {
						currentAccountChanges.acquisitionCosts[keyPath: valueKeyPath] = value
					}
				}
				else if components[0] == prefix_depreciations
				{
					if let valueKeyPath = depreciationKeypaths.first(where:{$0.0 == identifier})?.1 {
						currentAccountChanges.depreciations[keyPath: valueKeyPath] = value
					}
				}
				else if components[0] == prefix_bookvalues
				{
					if let valueKeyPath = bookValueKeypaths.first(where:{$0.0 == identifier})?.1 {
						currentAccountChanges.bookValues[keyPath: valueKeyPath] = value
					}
				}
			}
		}

		self.changes = changes
	}

}
