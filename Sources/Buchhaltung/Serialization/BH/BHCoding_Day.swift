// Copyright 2021-2023 Kai Oezer

import Foundation

extension Day
{
	public struct BHFormatStyle : ParseableFormatStyle
	{
		public func format(_ value: Day) -> String
		{
			value.description
		}

		public var parseStrategy : BHParseStrategy
		{
			BHParseStrategy()
		}
	}

	public struct BHParseStrategy : ParseStrategy
	{
		public func parse(_ value: String) throws -> Day
		{
			guard let day = Day(string: value) else { throw BHCodingError.invalidArchiveFormat }
			return day
		}
	}

	public var bh : String
	{
		self.formatted(BHFormatStyle())
	}
}

extension FormatStyle where Self == Day.BHFormatStyle
{
	public static var bhDay : Day.BHFormatStyle
	{
		Day.BHFormatStyle()
	}
}

extension ParseStrategy where Self == Day.BHParseStrategy
{
	public static var bhDay : Day.BHParseStrategy
	{
		Day.BHParseStrategy()
	}
}
