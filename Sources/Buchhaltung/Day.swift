// Copyright 2021-2025 Kai Oezer

import Foundation

public struct Day : Codable, Sendable
{
	private static let _timezone = TimeZone(identifier: "Europe/Berlin")!
	private static let _calendar : Calendar = {
		var cal = Calendar(identifier: .gregorian)
		cal.timeZone = _timezone
		return cal
	}()

	private static var _isoDateFormatter : ISO8601DateFormatter {
		let formatter = ISO8601DateFormatter()
		formatter.formatOptions = [.withInternetDateTime] // RFC 3339
		formatter.timeZone = _timezone
		return formatter
	}

	private static var _shortISODateFormatter : ISO8601DateFormatter {
		let formatter = ISO8601DateFormatter()
		formatter.formatOptions = [.withFullDate, .withDashSeparatorInDate]
		formatter.timeZone = _timezone
		return formatter
	}

	public let midday : Date
	public let start : Date
	public let end : Date

	private static let _distantPastDate = _calendar.date(from: .init(year: 2003, month: 12, day: 31, hour: 23, minute: 59, second: 59))!
	private static let _distantFutureDate = _calendar.date(from: .init(year: 2200, month: 1, day: 1, hour: 12))!

	public static let distantPast = Day(date: _distantPastDate)
	public static let distantFuture = Day(date: _distantFutureDate)

	public init(date : Date = Date())
	{
		if date >= Self._distantFutureDate
		{
			midday = Self._distantFutureDate
			start = midday
			end = midday
		}
		else if date <= Self._distantPastDate
		{
			midday = Self._distantPastDate
			start = midday
			end = midday
		}
		else
		{
			let firstSecond = Self._calendar.date(bySettingHour: 0, minute: 0, second: 0, of: date)!
			let startDate = Self._calendar.date(bySetting: .nanosecond, value: 0, of: firstSecond)
			assert(startDate != nil)
			start = startDate!

			let lastSecond = Self._calendar.date(bySettingHour: 23, minute: 59, second: 59, of: date)!
			let maxSubsecond = 999_999_000 // Date comparison seems to have microsecond precision
			let endDate = Self._calendar.date(bySetting: .nanosecond, value: maxSubsecond, of: lastSecond)
			assert(endDate != nil)
			end = endDate!

			let noon = Self._calendar.date(bySettingHour: 12, minute: 0, second: 0, of: date)!
			let middayDate = Self._calendar.date(bySetting: .nanosecond, value: 0, of: noon)
			assert(middayDate != nil)
			midday = middayDate!
		}
	}

	public init?(_ year: Int, _ month: Int, _ day: Int)
	{
		let components = DateComponents(
			calendar: Self._calendar,
			year: year,
			month: month,
			day: day,
			hour: 12,
			minute: 0,
			second: 0
		)
		guard components.isValidDate, let date = components.date else { return nil }
		self.init(date: date)
	}

	public init?(string: String)
	{
		guard
			let date = Self._shortISODateFormatter.date(from: string),
			let date = Self._calendar.date(bySettingHour: 12, minute: 0, second: 0, of: date)
			else { return nil }
		self.init(date: date)
	}

	public init?(internetDate string : String)
	{
		guard let date = Self._isoDateFormatter.date(from: string) else { return nil }
		self.init(date: date)
	}

	public var interval : DateInterval
	{
		DateInterval(start: self.start, end: self.end)
	}

	public var dateComponents : DateComponents
	{
		Self._calendar.dateComponents([.year, .month, .day], from: midday)
	}

	public var year : Int
	{
		Self._calendar.dateComponents([.year], from: midday).year ?? 0
	}

	public var month : Int
	{
		Self._calendar.dateComponents([.month], from: midday).month ?? 0
	}

	public var dayOfMonth : Int
	{
		Self._calendar.dateComponents([.day], from: midday).day ?? 0
	}

	public var isEndOfMonth : Bool
	{
		(self + 1).dayOfMonth == 1
	}

	public func contains(_ date : Date) -> Bool
	{
		self.interval.contains(date)
	}

	public func dateComponents(to day : Day) -> DateComponents
	{
		Self._calendar.dateComponents([.year, .month, .day], from: self.midday, to: day.midday)
	}

	public var isValid : Bool
	{
		self > .distantPast && self < .distantFuture
	}

	/// - returns: the same day in the following year
	public var nextYear : Day
	{
		guard self != .distantFuture && self != .distantPast else { return self }
		guard let newDate = Self._calendar.date(byAdding: .year, value: 1, to: midday) else { return self }
		return Day(date: newDate)
	}
}

extension Day : CustomStringConvertible
{
	public var description: String
	{
		Self._shortISODateFormatter.string(from:midday)
	}
}

extension Day : Hashable
{
	public func hash(into hasher: inout Hasher)
	{
		hasher.combine(midday)
	}
}

extension Day : Equatable
{
	public static func == (lhs : Day, rhs : Day) -> Bool
	{
		lhs.midday == rhs.midday
	}
}

extension Day : Comparable
{
	public static func < (lhs: Day, rhs: Day) -> Bool
	{
		lhs.end.compare(rhs.start) == .orderedAscending
	}

	public static func <= (lhs: Day, rhs: Day) -> Bool
	{
		!(lhs > rhs)
	}

	public static func > (lhs : Day, rhs : Day) -> Bool
	{
		lhs.start.compare(rhs.end) == .orderedDescending
	}

	public static func >= (lhs : Day, rhs : Day) -> Bool
	{
		!(lhs < rhs)
	}
}

extension Day
{
	public static func <= (day : Day, date : Date) -> Bool
	{
		let comparison = day.start.compare(date)
		return comparison == .orderedSame || comparison == .orderedAscending
	}

	public static func >= (day : Day, date : Date) -> Bool
	{
		let comparison = day.end.compare(date)
		return comparison == .orderedSame || comparison == .orderedDescending
	}
}

extension Day
{
	public static func + (day : Day, numDays : Int) -> Day
	{
		Day(date: _add(numDays, to: day))
	}

	public static func - (day : Day, numDays : Int) -> Day
	{
		Day(date: _add(-numDays, to: day))
	}

	private static func _add(_ numDays : Int, to day : Day) -> Date
	{
		Self._calendar.date(byAdding: .day, value: numDays, to: day.midday) ?? day.midday
	}
}

extension Day
{
	func formatted<T : FormatStyle>(_ formatStyle : T) -> String where T.FormatInput == Day, T.FormatOutput == String
	{
		formatStyle.format(self)
	}

	init<T : ParseStrategy>(_ value : String, strategy : T) throws where T.ParseInput == String, T.ParseOutput == Day
	{
		self = try strategy.parse(value)
	}
}
