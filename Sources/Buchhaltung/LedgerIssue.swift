// Copyright 2022-2024 Kai Oezer

import DEGAAP
import IssueCollection

extension IssueDomain
{
	public static let ledger : IssueDomain = "Ledger"
}

extension IssueCode
{
	/// The used taxonomy version may be incorrect.
	public static let unexpectedTaxonomyVersion : IssueCode = 5

	/// The journal contains an entry on the given day for the given fixed asset account that is not a supported account.
	public static let journalFixedAssetAccountNotSupported : IssueCode = 10

	/// The journal contains an entry on the given day for a fixed asset that could not be associated with a fixed asset in the inventory.
	public static let journalFixedAssetNotListed : IssueCode = 11

	/// Auto-depreciation of assets retired on the given day is not balanced by journal records for the same day.
	public static let retiredFixedAssetsNotAppreciatedInJournal : IssueCode = 12
	
	public static let journalChartMappingNotSupported : IssueCode = 20
}

extension IssueMetadataKey
{
	/// value type ``Day``
	public static let day : IssueMetadataKey = "day"

	/// value type ``AccountID``
	public static let accountID : IssueMetadataKey = "account_ID"

	/// value type: ``DEGAAPTaxonomyVersion``
	public static let taxonomy : IssueMetadataKey = "degaap_taxonomy"

	/// value type: ``String``
	public static let assetRegistrationID : IssueMetadataKey = "asset_registration_ID"

	/// value type: ``DEGAAP.DEGAAPMappingSourceChart``
	public static let chartMappingSource : IssueMetadataKey = "mapping_source_chart"
}
