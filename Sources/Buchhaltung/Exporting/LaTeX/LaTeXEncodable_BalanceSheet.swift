// Copyright 2020-2023 Kai Oezer

import Foundation

extension BalanceSheet : LaTeXEncodable
{
	public func latexRepresentation() throws -> String
	{
		var template = try LaTeXEncoder.contents(ofTemplate: "bilanz")

		let assetValues : (BalanceSheet.Assets) -> [Money] = {[
			$0.fixedAssets,
			$0.currentAssets,
			$0.deferrals.monetaryValue,
			$0.deferredTax.monetaryValue,
			$0.surplus.monetaryValue,
			$0.sum
		]}
		let equityLiabilitesValues : (BalanceSheet.EquityAndLiabilities) -> [Money] = {[
			$0.equity,
			$0.provisions,
			$0.liabilities,
			$0.deferrals.monetaryValue,
			$0.deferredTax.monetaryValue,
			$0.sum
		]}
		let values = assetValues(self.assets) + equityLiabilitesValues(self.eqLiab)
		let prevYearValues = assetValues(self.previousPeriodAssets) + equityLiabilitesValues(self.previousPeriodEqLiab)
		let valuePlaceholders = [
			"BilAkAV",
			"BilAkUV",
			"BilAkRAP",
			"BilAkLS",
			"BilAkAUB",
			"BilAkSum",
			"BilPaEK",
			"BilPaRS",
			"BilPaVB",
			"BilPaRAP",
			"BilPaLS",
			"BilPaSum"
		]
		let prevYearValuePlaceholders = valuePlaceholders.map{ "\($0).Prev" }

		do
		{
			for (placeholder, value) in zip(valuePlaceholders + prevYearValuePlaceholders, values + prevYearValues)
			{
				try LaTeXEncoder.replace(placeholder: placeholder, in: &template, with: value.latex)
			}
		}
		catch
		{
			throw LaTeXEncodingError.generalError("Error while generating the LaTeX output of the balance sheet: \(error)")
		}
		return template
	}
}
