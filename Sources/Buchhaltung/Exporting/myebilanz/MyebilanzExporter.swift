// Copyright 2022-2025 Kai Oezer

import Foundation
import DEGAAP

public enum MyebilanzExportingError : Error, CustomStringConvertible
{
	case missingTemplate
	case missingData(String)

	public var description: String
	{
		switch self
		{
			case .missingData(let message): return message
			case .missingTemplate: return "missing myebilanz INI template"
		}
	}
}

/// Generates the contents for a myebilanz INI file and a CSV file that lists account totals.
///
/// The generated INI file content needs to be manually edited to insert
/// * the name and address of the data transferring person (Transferdatenlieferant),
/// * the name and address of the data supplying person (Nutzdatenlieferant),
/// * the complete path to the Elster user certification file,
/// * the Elster log-in password,
/// * the complete path to the generated CSV file with the account totals.
public struct MyebilanzExporter
{
	static let logger = Logger(category: "myebilanz")

	/// - returns: the contents of the myebilanz INI file with embedded account values
	public static func export(report : Report, baseReport : Report? = nil) throws -> String
	{
		let (iniContentsWithoutAccounts, accountTotals) = try _export(report: report, baseReport: baseReport)
		let sortedAccountValueMapping = accountTotals.totals.sorted{$0.key < $1.key}
		let accountValueLines = sortedAccountValueMapping.map{"de-gaap-ci:\($0.key)=\($0.value.myebilanz)"}
		return iniContentsWithoutAccounts
			.replacingOccurrences(of: "#FILENAME#", with: "INI")
			.replacingOccurrences(of: "#ACCOUNTS#", with: accountValueLines.joined(separator: "\n"))
	}

	/// - returns: A tuple with the contents for the INI and CSV files, respectively
	public static func exportWithCSV(report : Report, baseReport : Report? = nil) throws -> (String, String)
	{
		let (iniContentsWithoutAccounts, accountTotals) = try _export(report: report, baseReport: baseReport)
		let enumeratedAccounts = accountTotals.totals.sorted{$0.key < $1.key}.enumerated()
		let accountReferenceLines = enumeratedAccounts.map{"de-gaap-ci:\($0.element.key)=\($0.offset + 1)"}

		let iniContents = iniContentsWithoutAccounts
			.replacingOccurrences(of: "#FILENAME#", with: "<path>\\<csv file name>")
			.replacingOccurrences(of: "#ACCOUNTS#", with: accountReferenceLines.joined(separator: "\n"))

		let accountTotalsContent = enumeratedAccounts.map {
			"\($0.offset + 1); \($0.element.value.myebilanz); de-gaap-ci:\($0.element.key.description); \"-\""
		}.joined(separator: "\n")

		return (iniContents, accountTotalsContent)
	}

	private static func _export(report : Report, baseReport : Report? = nil) throws -> (String, AccountTotals)
	{
		guard let iniTemplateLocation = Bundle.module.url(forResource: "myebilanz_ini_csv", withExtension: "txt"),
			let iniTemplate = try? String(contentsOf: iniTemplateLocation, encoding: .utf8) else {
			throw MyebilanzExportingError.missingTemplate
		}
		let taxonomyVersion = (DEGAAPTaxonomyVersion(date: report.period.end.midday) ?? .allCases.last!)
		let needsToReportBVV = taxonomyVersion.traits.contains(.bvv)
		let totalsWithoutNetIncomeTotal = AccountTotals(totals: report.accountTotals.totals.filter{ $0.key != DEGAAPAccounts.eqLiab_equity_netIncome })
		let accountTotals = [
			needsToReportBVV ? report.bvv : nil,
			totalsWithoutNetIncomeTotal,
			report.incomeUse
		].compactMap{ $0 }.reduce(AccountTotals()){ $0 + $1 }

		let reportElements = ["-GuV", "GuVMicroBilG", "B", "-SGE", "-KS", "-STU", "-EB", "-SGEP", "-KKE", "-SA", "-AV"]
			+ (report.notes.assetChangesStatement != nil ? ["BAL"] : ["-BAL"])
			+ (report.incomeUse != nil ? ["EV"] : ["-EV"])
			+ (needsToReportBVV ? ["BVV"] : ["-BVV"])

		let iniContentsWithoutAccounts = iniTemplate.trimmingCharacters(in: .whitespacesAndNewlines)
			.replacingOccurrences(of: "#PERIOD_END#", with: report.period.end.description)
			.replacingOccurrences(of: "#REPORT_ELEMENTS#", with: reportElements.joined(separator: ","))
			.replacingOccurrences(of: "#WITH_BALANCE_PROFIT#", with: report.incomeUse != nil ? "true" : "false")
			.replacingOccurrences(of: "#TAXONOMY#", with: taxonomyVersion.myebilanz)
			.replacingOccurrences(of: "#COMPANY_INFO#", with: report.notesBelowBalanceSheet.companyInformation.myebilanz)
			.replacingOccurrences(of: "#ASSET_CHANGES#", with: _assetChanges(for: report.notes.assetChangesStatement))
			.replacingGermanCharacters

		return (iniContentsWithoutAccounts, accountTotals)
	}

	private static func _assetChanges(for statement : AssetChangesStatement?) -> String
	{
		guard let statement else { logger.warning("Exporting report without asset changes statement."); return "" }
		return statement.changes.sorted{ $0.key < $1.key }.map {
			let account = $0.key
			let accountChangeStatement = $0.value
			return """
			; \(account)
			de-gaap-ci:\(account)!de-gaap-ci:grossCost.beginning="[\(accountChangeStatement.acquisitionCosts.begin.myebilanz)]"
			de-gaap-ci:\(account)!de-gaap-ci:gross.addition="[\(accountChangeStatement.acquisitionCosts.new.myebilanz)]"
			de-gaap-ci:\(account)!de-gaap-ci:gross.release="[\(accountChangeStatement.acquisitionCosts.retirements.myebilanz)]"
			de-gaap-ci:\(account)!de-gaap-ci:gross.movements="[\(accountChangeStatement.acquisitionCosts.rebookings.myebilanz)]"
			de-gaap-ci:\(account)!de-gaap-ci:accDepr.beginning="[\(accountChangeStatement.depreciations.begin.myebilanz)]"
			de-gaap-ci:\(account)!de-gaap-ci:accDepr.DeprPeriod.regular="[\((accountChangeStatement.depreciations.end - accountChangeStatement.depreciations.begin).myebilanz)]"
			de-gaap-ci:\(account)!de-gaap-ci:all_Prev_period="[\((accountChangeStatement.acquisitionCosts.begin - accountChangeStatement.depreciations.begin).myebilanz)]"
			"""
		}.joined(separator: "\n")
	}
}

extension String
{
	var replacingGermanCharacters : String
	{
		var tmp = self
		[("Ä", "Ae"), ("ä", "ae"), ("Ö", "Oe"), ("ö", "oe"), ("Ü", "Ue"), ("ü", "ue"), ("ß", "ss")].forEach {
			tmp = tmp.replacingOccurrences(of: $0.0, with: $0.1)
		}
		return tmp
	}
}

extension Money
{
	var myebilanz : String
	{
		self.formatted(MyebilanzFormatStyle())
	}

	struct MyebilanzFormatStyle : FormatStyle
	{
		func format(_ value: Money) -> String
		{
			value.decimalStringRepresentation(
				usingCommaAsDecimalSeparator: false,
				withThousandsSeparator: false,
				suppressingZeroSubamounts: true)
		}
	}
}

extension DEGAAPTaxonomyVersion
{
	var myebilanz : String
	{
		humanReadable
	}
}
