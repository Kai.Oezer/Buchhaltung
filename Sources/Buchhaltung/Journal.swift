// Copyright 2020-2025 Kai Oezer

import Foundation
import DEGAAP
import IssueCollection
import OrderedCollections

extension DEGAAPSourceChart
{
	public static var `default` : DEGAAPSourceChart { .skr03 }
}

/// An accounting records journal that supports grouping and mapping from another account chart to the DEGAAP chart.
///
/// German: Buchungsjournal, Geschaeftsbuch
/// - Tag: journal
public struct Journal : Sendable
{
	public var records = [JournalRecord]()

	/// the accounts chart used in the journal records
	public var chart : DEGAAPSourceChart

	public init(records : [JournalRecord] = [], chart : DEGAAPSourceChart = .default)
  {
		self.records = records
		self.chart = chart
  }

	public var flattened : Journal
	{
		var nonGroupRecords = [JournalRecord]()
		for record in records {
			_collectNonGroupRecords(in: record, collection: &nonGroupRecords)
		}
		return Journal(records: nonGroupRecords, chart: self.chart)
	}

	/// - Returns: A new Journal instance, where the account charts are mapped to DEGAAP charts.
	///
	/// Call this function on a ``flattened`` Journal
	func degaap(
		for chartMapping : DEGAAPMappingDescriptor,
		using degaap : DEGAAPEnvironment
	) async -> DEGAAPJournal?
	{
		var mappedRecords = [DEGAAPRecord]()
		mappedRecords.reserveCapacity(records.count)
		
		let mapRecord : (JournalRecord, KeyPath<JournalRecord, OrderedSet<JournalRecord.Entry>>) async -> [DEGAAPRecord.Entry] = { record, keyPath in
			var mappedEntries = [DEGAAPRecord.Entry]()
			for entry in record[keyPath: keyPath] {
				if let accountID = await degaap.chartItem(for: entry.account, mapping: chartMapping) {
					mappedEntries.append(Record.Entry(account: accountID, value: entry.value))
				}
			}
			return mappedEntries
		}

		for record in records {
			let mappedDebits = await mapRecord(record, \.debit)
			let mappedCredits = await mapRecord(record, \.credit)
			let mappedRecord = DEGAAPRecord(date: record.date, title: record.title, tags: record.tags, debit: OrderedSet(mappedDebits), credit: OrderedSet(mappedCredits))
			mappedRecords.append(mappedRecord)
		}
		return DEGAAPJournal(records: mappedRecords)
	}

	/// Groups the records with the given IDs.
	/// Does not group parent records if a descendent record is in the given set.
	public mutating func groupRecords(withIDs recordIDs : Set<JournalRecord.ID>)
	{
		guard recordIDs.count > 1,
			let groupedRecords = Buchhaltung.groupRecords(withIDs: recordIDs, in: self.records)
			else { return }
		self.records = groupedRecords
	}

	/// Ungroups the records with the given record IDs.
	///
	/// Ungroups descendent records first.
	/// If any descendent record was ungrouped, none of the root records will be ungrouped.
	public mutating func ungroupRecords(withIDs recordIDs : Set<JournalRecord.ID>)
	{
		if let ungroupedRecords = Buchhaltung.ungroupRecords(withIDs: recordIDs, in: records) {
			self.records = ungroupedRecords
		}
	}

	private func _collectNonGroupRecords(in record : JournalRecord, collection : inout [JournalRecord])
	{
		if let subRecords = record.subRecords {
			for subRecord in subRecords {
				_collectNonGroupRecords(in: subRecord, collection: &collection)
			}
		} else {
			collection.append(record)
		}
	}
}

extension Journal : Equatable
{
	public static func == (lhs : Journal, rhs : Journal) -> Bool
	{
		guard lhs.chart == rhs.chart else { return false }
		guard lhs.records.count == rhs.records.count else { return false }
		return lhs.records == rhs.records
	}
}

extension Journal : Codable
{
	public init(from decoder: Decoder) throws
	{
		assertionFailure("Journal can only be serialized using TOMLikeCoder")
		chart = .default
	}

	public func encode(to encoder: Encoder) throws
	{
		assertionFailure("Journal can only be deserialized using TOMLikeCoder")
	}
}

/// A flattened journal that uses DEGAAP accounts directly.
struct DEGAAPJournal
{
	var records = [DEGAAPRecord]()

	public mutating func add(_ newRecords : [DEGAAPRecord])
	{
		records.append(contentsOf: newRecords)
	}

	public func journal(byAdding otherRecords : [DEGAAPRecord]) -> DEGAAPJournal
	{
		DEGAAPJournal(records: self.records + otherRecords)
	}

	public var accountTotals : AccountTotals
	{
		var accountTotals = [AccountID : Money]()
		self.records.forEach { record in
			record.debit.forEach {
				accountTotals[$0.account] = (accountTotals[$0.account] ?? .zero) + $0.value
			}
			record.credit.forEach {
				accountTotals[$0.account] = (accountTotals[$0.account] ?? .zero) - $0.value
			}
		}
		return AccountTotals(totals: accountTotals)
	}

}
