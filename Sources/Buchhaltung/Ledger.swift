// Copyright 2020-2025 Kai Oezer
// swiftlint:disable file_length

import Foundation
import DEGAAP
import IssueCollection
import TOMLike

public struct Ledger : Sendable
{
	public var period : Period
	public var taxonomy : DEGAAPTaxonomyVersion
	public var company : Company
	public var journal : Journal
	public var assets : Assets
	public var debts : Debts
	public var baseReport : Report?

  public init(
		period : Period,
		taxonomy : DEGAAPTaxonomyVersion? = nil,
		company : Company,
		journal : Journal? = nil,
		assets : Assets? = nil,
		debts : Debts? = nil,
		basedOn report : Report? = nil
	)
  {
		self.period = period
		self.taxonomy = taxonomy ?? (DEGAAPTaxonomyVersion(date: period.end.midday) ?? .allCases.last!)
		self.company = company
		self.journal = journal ?? Journal()
		self.assets = assets ?? Assets()
		self.debts = debts ?? Debts()
		self.baseReport = report
  }

	public func generateReport(
		type: ReportType,
		title : String? = nil,
		withInventory : Bool = false,
		using degaap : DEGAAPEnvironment,
		issues : inout IssueCollection
	) async throws -> Report
  {
		guard (baseReport == nil) || (baseReport!.type == type) else {
			throw ReportError.invalidBaseReportType(type)
		}
		let chart = DEGAAPChartDescriptor(taxonomy: taxonomy, fiscal: type == .fiscal)
		guard await degaap.contains(chart: chart) else {
			throw ReportError.invalidChart("\(type == .fiscal ? "Fiscal" : "Trade") chart not available for taxonomy \(taxonomy).")
		}
		let title = title ?? Report.defaultTitle(for: type, period: period)
		let mappedJournal = await journalByResolvingAccountMapping(of: journal, using: degaap, issues: &issues)
		let initializedJournal = mappedJournal.journal(byAdding: _initializationRecords())
		let journalWithDepreciations = initializedJournal.journal(byAdding: _generateFixedAssetDepreciationRecords(from:assets))
		var extendedInventory = assets
		if let derivedReceivable = _derivedReceivableAsset(from: initializedJournal)
		{
			extendedInventory.currentAssets.insert(derivedReceivable)
		}
		let inventory = Inventory(period: period, assets: extendedInventory, debts: debts)
		let incomeStatement = await _generateIncomeStatement(chart: chart, accountTotals: journalWithDepreciations.accountTotals, inventory: inventory, using: degaap)
		let journalWithProfitLoss = journalWithDepreciations.journal(byAdding: [_profitLossRecord(from: incomeStatement)])
		let balanceSheetAccountTotals = journalWithProfitLoss.accountTotals
		let balanceSheet = await _generateBalanceSheet(chart: chart, accountTotals: balanceSheetAccountTotals, incomeStatement: incomeStatement, using: degaap)
		let notes = _generateNotes(type: type)
		let notesBelowBalanceSheet = NotesBelowBalanceSheet(companyInformation: self.company, incomeCalculationMethod: .gkv)
		let bvv = await _generateBVV(from: balanceSheet, chart: chart, accountTotals: balanceSheetAccountTotals, baseReport: baseReport, using: degaap)

		await _checkFixedAssetsUsedInJournal(using: degaap, issues: &issues)

		return Report(
			title: ReportTitle(title),
			type: type,
			taxonomy: taxonomy,
			period: self.period,
			previousPeriod: baseReport?.period,
			inventory: inventory,
			incomeStatement: incomeStatement,
			balanceSheet: balanceSheet,
			notesBelowBalanceSheet: notesBelowBalanceSheet,
			notes: notes,
			accountTotals: balanceSheetAccountTotals,
			bvv: bvv,
			exportsInventory: withInventory
		)
	}

	/// - returns: the total value accumulated on the given account
	/// from the start of the journal up to, and including, the given day
	public func accountTotal(for accountID : AccountID, upTo date : Day, using degaap : DEGAAPEnvironment) async -> Money
	{
		var issues = IssueCollection()
		let mappedJournal = await journalByResolvingAccountMapping(of: journal, using: degaap, issues: &issues)
		var total = Money.zero
		for record in mappedJournal.records {
			if record.date <= date {
				total = total
					+ record.debit.filter{ $0.account == accountID }.reduce(0){ $0 + $1.value }
					- record.credit.filter{ $0.account == accountID }.reduce(0){ $0 + $1.value }
			}
		}
		return total
	}

	private func _derivedReceivableAsset(from journal : DEGAAPJournal) -> Assets.CurrentAsset?
	{
		var receivable : Money = .zero
		for record in journal.records
		{
			receivable += record.debit.filter{$0.account.name.hasPrefix("bs.ass.currAss.receiv.")}.reduce(.zero){$0 + $1.value}
			receivable -= record.credit.filter{$0.account.name.hasPrefix("bs.ass.currAss.receiv.")}.reduce(.zero){$0 + $1.value}
		}
		return receivable != .zero ? Assets.CurrentAsset(type:.receivable, name: "derived", value: receivable) : nil
	}

	/// Initializes the journal with the values from the balance sheet of the base report.
	private func _initializationRecords() -> [DEGAAPRecord]
	{
		var records = [DEGAAPRecord]()
		guard let baseReport = baseReport else { return records }

		var totals = baseReport.accountTotals.totals.filter {
			let descr = $0.key.description
			return !descr.starts(with: "is.netIncome.") && !descr.starts(with: "bs.eqLiab.")
		}

		// Equity-liability values taken from ``BalanceSheet`` are multiplied with -1
		// because credit-side values are mapped to negative values in AccountTotals.
		totals[DEGAAPAccounts.eqLiab_accruals_init] = -1 * baseReport.balanceSheet.eqLiab.provisions
		totals[DEGAAPAccounts.eqLiab_liab_init] = -1 * baseReport.balanceSheet.eqLiab.liabilities
		totals[DEGAAPAccounts.eqLiab_defIncome_init] = -1 * baseReport.balanceSheet.eqLiab.deferrals.monetaryValue
		totals[DEGAAPAccounts.eqLiab_defTax_init] = -1 * baseReport.balanceSheet.eqLiab.deferredTax.monetaryValue

		// equity-liability values taken directly from the base report account totals
		totals[DEGAAPAccounts.eqLiab_equity_subscribed] = baseReport.accountTotals.totals[DEGAAPAccounts.eqLiab_equity_subscribed]
		totals[DEGAAPAccounts.eqLiab_equity_retainedEarnings] = (baseReport.accountTotals.totals[DEGAAPAccounts.eqLiab_equity_retainedEarnings] ?? .zero) + (baseReport.accountTotals.totals[DEGAAPAccounts.eqLiab_equity_netIncome] ?? .zero)
		totals[DEGAAPAccounts.eqLiab_equity_profitLoss_retainedEarnings] = (baseReport.accountTotals.totals[DEGAAPAccounts.eqLiab_equity_profitLoss_retainedEarnings] ?? .zero) + (baseReport.accountTotals.totals[DEGAAPAccounts.eqLiab_equity_profitLoss] ?? .zero)

		totals.forEach { accountID, value in
			let entry = Record.Entry(account: accountID, value: value.absolute)
			let isDebit = value > 0
			records.append( DEGAAPRecord(date: period.start, title: "init \(accountID)", debit: isDebit ? [entry] : [], credit: isDebit ? [] : [entry]) )
		}
		return records
	}

	func journalByResolvingAccountMapping(
		of journal : Journal,
		using degaap : DEGAAPEnvironment,
		issues : inout IssueCollection
	) async -> DEGAAPJournal
	{
		let targetChart = DEGAAPChartDescriptor(taxonomy: taxonomy, fiscal: true)
		let mapping = DEGAAPMappingDescriptor(source: journal.chart, target: targetChart)
		guard let degaapResolvedJournal = await journal.degaap(for: mapping, using: degaap) else {
			issues.append(Issue(domain: .ledger, code: .journalChartMappingNotSupported, message: "Journal uses unsupported chart mapping: \(journal.chart.bh) -> Taxonomy \(taxonomy)"))
			return DEGAAPJournal()
		}
		return degaapResolvedJournal
	}

	private func _generateFixedAssetDepreciationRecords(from inventory : Assets) -> [DEGAAPRecord]
	{
		var depreciationRecords = [DEGAAPRecord]()
		var counter = 1
		for fixedAsset in inventory.fixedAssets
		{
			guard fixedAsset.depreciationSchedule.progression != .none,
				fixedAsset.acquisitionDate <= period.end,
				fixedAsset.retirementDate >= period.start
				else { continue }

			let depreciationValue = fixedAsset.bookValue(for: period.start - 1) - fixedAsset.bookValue(for: period.end)
			let debit = Record.Entry(account: fixedAsset.isGWG ? DEGAAPAccounts.income_deprAmort_gwg : DEGAAPAccounts.income_deprAmort_tangible, value: depreciationValue)
			let credit = Record.Entry(account: fixedAsset.account, value: depreciationValue)
			depreciationRecords.append(Record(date: period.end, title: "auto-generated total fixed asset depreciation #\(counter)", debit: [debit], credit: [credit]))
			counter += 1
		}
		return depreciationRecords
	}

	private func _profitLossRecord(from incomeStatement : IncomeStatement) -> DEGAAPRecord
	{
		let credit = Record.Entry(account: DEGAAPAccounts.eqLiab_equity_netIncome, value: incomeStatement.results.profitLoss)
		return Record(date: period.end, title: "auto-generated total profit or loss", debit: [], credit: [credit])
	}

	private func _generateIncomeStatement(
		chart : DEGAAPChartDescriptor,
		accountTotals : AccountTotals,
		inventory : Inventory,
		using degaap : DEGAAPEnvironment
	) async -> IncomeStatement
	{
		var incomeStatement = IncomeStatement()
		if let previousResults = baseReport?.incomeStatement.results
		{
			incomeStatement.previousYearResults = previousResults
		}
		var results = IncomeStatement.Results()
		let total : (AccountID) async -> Money = { await _calculateTotal(for: $0, from: accountTotals, section: .incomeStatement, chart: chart, using: degaap) }
		results.earnings      = await total(DEGAAPAccounts.income_netSales).negated
		results.otherEarnings = await total(DEGAAPAccounts.income_otherOpRevenue).negated
		results.materialCosts = await total(DEGAAPAccounts.income_materialServices)
		results.salaries      = await total(DEGAAPAccounts.income_staff)
		results.depreciations = await total(DEGAAPAccounts.income_deprAmort)
		results.otherExpenses = await total(DEGAAPAccounts.income_otherCost)
		results.tax           = await total(DEGAAPAccounts.income_tax)
		incomeStatement.results = results
		return incomeStatement
	}

	private func _generateBalanceSheet(
		chart : DEGAAPChartDescriptor,
		accountTotals : AccountTotals,
		incomeStatement : IncomeStatement,
		using degaap : DEGAAPEnvironment
	) async -> BalanceSheet
	{
		var sheet = BalanceSheet()
		if let previousPeriodSheet = baseReport?.balanceSheet
		{
			sheet.previousPeriodAssets = previousPeriodSheet.assets
			sheet.previousPeriodEqLiab = previousPeriodSheet.eqLiab
		}
		let assetTotal : (AccountID) async -> Money = { await _calculateTotal(for: $0, from: accountTotals, section: .assets, chart: chart, using: degaap) }
		sheet.assets.fixedAssets = await assetTotal(DEGAAPAccounts.ass_fixed)
		sheet.assets.currentAssets = await assetTotal(DEGAAPAccounts.ass_current)
		sheet.assets.deferrals.monetaryValue = await assetTotal(DEGAAPAccounts.ass_prepaidExp)
		sheet.assets.deferredTax.monetaryValue = await assetTotal(DEGAAPAccounts.ass_defTax)
		sheet.assets.surplus.monetaryValue = await assetTotal(DEGAAPAccounts.ass_SurplusFromOffsetting)
		let eqLibTotal : (AccountID) async -> Money = { await _calculateTotal(for: $0, from: accountTotals, section: .equityAndLiabilities, chart: chart, using: degaap) }
		sheet.eqLiab.equity = await eqLibTotal(DEGAAPAccounts.eqLiab_equity).negated
		sheet.eqLiab.provisions = await eqLibTotal(DEGAAPAccounts.eqLiab_accruals)
		sheet.eqLiab.liabilities = await eqLibTotal(DEGAAPAccounts.eqLiab_liab).negated
		sheet.eqLiab.deferrals.monetaryValue = await eqLibTotal(DEGAAPAccounts.eqLiab_defIncome)
		sheet.eqLiab.deferredTax.monetaryValue = await eqLibTotal(DEGAAPAccounts.eqLiab_defTax).negated
		return sheet
	}

	/// Micro companies do not need to provide a notes section in their trade reports.
	/// Tax reports, on the other hand, require a Asset Changes Statement, which is part of the notes section.
	private func _generateNotes(type : ReportType) -> Notes
	{
		var notes = Notes()
		if type == .fiscal
		{
			notes.assetChangesStatement = _generateAssetChangesStatement()
		}
		return notes
	}

	private func _generateAssetChangesStatement() -> AssetChangesStatement
	{
		let processAcquistionCosts : (Assets.FixedAsset, inout AssetChangesStatement.AssetAccountChanges) -> Void = { asset, accountChanges in
			if asset.acquisitionDate < period.start
			{
				accountChanges.acquisitionCosts.begin += asset.acquisitionCost
			}

			if period.contains(asset.acquisitionDate)
			{
				accountChanges.acquisitionCosts.new += asset.acquisitionCost
			}

			// In this version, Buchhaltung does not support interest payments as acquisition costs.

			// In this version, Buchhaltung does not support rebookings of assets.

			if period.contains(asset.retirementDate)
			{
				accountChanges.acquisitionCosts.retirements += asset.acquisitionCost
			}
			else
			{
				accountChanges.acquisitionCosts.end += asset.acquisitionCost
			}
		}

		let processDepreciationsAndBookValues : (Assets.FixedAsset, inout AssetChangesStatement.AssetAccountChanges) -> Void = { asset, accountChanges in
			let depreciationDate = period.contains(asset.retirementDate) ? asset.retirementDate : period.end
			let assetEndValue = asset.depreciatedValue(for: depreciationDate)
			let assetTotalDepreciation = asset.acquisitionCost - assetEndValue

			if asset.acquisitionDate < period.start
			{
				let prevEndValue = asset.depreciatedValue(for: period.start - 1)
				accountChanges.depreciations.begin += asset.acquisitionCost - prevEndValue
				accountChanges.depreciations.new += prevEndValue - assetEndValue
			}
			else
			{
				accountChanges.depreciations.new += assetTotalDepreciation
			}

			if period.contains(asset.retirementDate)
			{
				accountChanges.depreciations.retirements += assetTotalDepreciation
			}
			else
			{
				accountChanges.depreciations.end += assetTotalDepreciation
			}

			accountChanges.bookValues.current += asset.bookValue(for: period.end)
		}

		var statement = AssetChangesStatement()

		for asset in assets.fixedAssets
		{
			let account = asset.account
			var accountChanges = AssetChangesStatement.AssetAccountChanges()

			if asset.acquisitionDate <= period.end
				&& asset.retirementDate >= period.start
			{
				processAcquistionCosts(asset, &accountChanges)
				processDepreciationsAndBookValues(asset, &accountChanges)
			}

			if let existingAccountChanges = statement.changes[account]
			{
				accountChanges.adding(existingAccountChanges)
			}

			statement.changes[account] = accountChanges
		}

		baseReport?.notes.assetChangesStatement?.changes.forEach {
			statement.changes[$0.key]?.bookValues.previous = $0.value.bookValues.current
		}

		return statement
	}

	private func _generateBVV(
		from balanceSheet : BalanceSheet,
		chart : DEGAAPChartDescriptor,
		accountTotals : AccountTotals,
		baseReport : Report?,
		using degaap : DEGAAPEnvironment
	) async -> AccountTotals
	{
		let withdrawals = await _calculateTotal(for: DEGAAPAccounts.ass_deficit, from: accountTotals, section: .assets, chart: chart, using: degaap)
		let profit = balanceSheet.eqLiab.equity - withdrawals
		return AccountTotals(totals: [
			DEGAAPAccounts.ass_deficit : withdrawals,
			DEGAAPAccounts.bvv_assets_current : -1 * profit,
			DEGAAPAccounts.bvv_assets_previous : baseReport?.bvv?.totals[DEGAAPAccounts.bvv_assets_current] ?? .zero,
			DEGAAPAccounts.bvv_withdrawals : .zero, // not supported
			DEGAAPAccounts.bvv_contributions : .zero // not supported
		])
	}

	private func _calculateTotal(
		for parentAccount : AccountID,
		from subAccountTotals : AccountTotals,
		section : DEGAAPReportSection,
		chart : DEGAAPChartDescriptor,
		using degaap : DEGAAPEnvironment
	) async -> Money
	{
		let childAccounts : [AccountID] = subAccountTotals.totals.keys.map{ $0 }
		guard let weights : [DEGAAPWeight] = await degaap.weights(for: parentAccount, children: childAccounts, chart: chart, section: section) else { return Money.zero }
		return zip(weights, childAccounts).map{ $0.0.rawValue * subAccountTotals.totals[$0.1]! }.reduce(Money.zero){ $0 + $1 }
	}
}

extension Ledger : Codable
{
	enum Format : Int, Codable, TOMLikeEmbeddable
	{
		case v1 = 1

		public func tomlikeEmbedded() throws -> String
		{
			String(self.rawValue)
		}

		public init(tomlikeEmbedded lines : [IndexedLine], issues : inout IssueCollection) throws
		{
			guard let content = lines.first?.content,
				let version = Int(content),
				let f = Format(rawValue:version)
				else { throw BHCodingError.invalidArchiveFormat }
			self = f
		}
	}

	enum CodingKeys : String, CodingKey
	{
		case format
		case period
		case taxonomy
		case company
		case assets
		case debts
		case journal
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(Format.v1, forKey: .format)
		try container.encode(period, forKey: .period)
		try container.encode(taxonomy, forKey: .taxonomy)
		try container.encode(company, forKey: .company)
		try container.encode(assets, forKey: .assets)
		try container.encode(debts, forKey: .debts)
		try container.encode(journal, forKey: .journal)
	}

	public init(from decoder: Decoder) throws
	{
		let container = try decoder.container(keyedBy: CodingKeys.self)
		let format = try container.decode(Format.self, forKey: .format)
		guard format == .v1 else { throw DecodingError.dataCorruptedError(forKey: CodingKeys.format, in: container, debugDescription: "unsupported or invalid file format") }
		self.period = try container.decode(Period.self, forKey: .period)
		self.taxonomy = try container.decodeIfPresent(DEGAAPTaxonomyVersion.self, forKey: .taxonomy) ?? (DEGAAPTaxonomyVersion.init(date: self.period.end.midday) ?? .allCases.last!)
		self.company = try container.decode(Company.self, forKey: .company)
		self.assets = try container.decodeIfPresent(Assets.self, forKey: .assets) ?? Assets()
		self.debts = try container.decodeIfPresent(Debts.self, forKey: .debts) ?? Debts()
		self.journal = try container.decodeIfPresent(Journal.self, forKey: .journal) ?? Journal()
	}
}

extension Ledger : Archivable
{
	public init(fromArchive archive : String, issues : inout IssueCollection) throws
	{
		let ledger = try TOMLikeCoder.decode(Self.self, from: archive, issues: &issues)
		self.init(period: ledger.period, company: ledger.company, journal: ledger.journal, assets: ledger.assets, debts: ledger.debts)
	}

	public func archive() throws -> String
	{
		try TOMLikeCoder.encode(self)
	}
}

extension Ledger
{
	public func checkForIssues(using degaap : DEGAAPEnvironment, issues : inout IssueCollection) async
	{
		_checkTaxonomyVersion(&issues)
		await _checkFixedAssetsUsedInJournal(using: degaap, issues: &issues)
		await _checkRetiredFixedAssetsAppreciatedInJournal(using: degaap, issues: &issues)
	}

	private func _checkTaxonomyVersion(_ issues : inout IssueCollection)
	{
		if taxonomy != DEGAAPTaxonomyVersion(date: period.end.midday) {
			issues.append(Issue(domain: .ledger, code: .unexpectedTaxonomyVersion))
		}
	}

	private func _checkFixedAssetsUsedInJournal(using degaap : DEGAAPEnvironment, issues : inout IssueCollection) async
	{
		let fixedAssetRegistrationIDs = assets.fixedAssets.map{ $0.registrationID }
		let accountsJournal = await journalByResolvingAccountMapping(of: journal, using: degaap, issues: &issues).records
		for record in accountsJournal
		{
			for entry in record.debit
			{
				if entry.account.isFixedAssetAccountID
				{
					if DEGAAPAccounts.inventoryFixedAssetAccounts.contains( entry.account )
					{
						let recordDescription = record.title
						let assetIsReferencedInRecordDescription = assets.fixedAssets.contains{ recordDescription.contains($0.name) }
						let assetIsReferencedInRecordTags = record.tags.map{ $0.trimmingCharacters(in: CharacterSet(charactersIn:"#")) }.reduce(false){ $0 || fixedAssetRegistrationIDs.contains($1) }
						let foundMatchingInventoryItem = assetIsReferencedInRecordDescription || assetIsReferencedInRecordTags
						if !foundMatchingInventoryItem
						{
							issues.append(Issue(domain: .ledger, code: .journalFixedAssetNotListed, metadata: [.day : record.date.bh, .accountID : entry.account.description]))
						}
					}
					else
					{
						issues.append(Issue(domain: .ledger, code: .journalFixedAssetAccountNotSupported, metadata: [.day : record.date.bh, .accountID : entry.account.description]))
					}
				}
			}
		}
	}

	private func _checkRetiredFixedAssetsAppreciatedInJournal(using degaap : DEGAAPEnvironment, issues : inout IssueCollection) async
	{
		let retiredAssets = assets.fixedAssets.filter({ period.contains($0.retirementDate) })
		guard !retiredAssets.isEmpty else { return }
		let resolvedJournalRecords = await journalByResolvingAccountMapping(of: journal, using: degaap, issues: &issues).records
		let retirementDays = Set<Day>(retiredAssets.map{$0.retirementDate})
		for retirementDay in retirementDays
		{
			let recordsOnRetirementDay = resolvedJournalRecords.filter{$0.date == retirementDay}
			let recordsAppreciationValue = recordsOnRetirementDay.reduce(.zero, {$0 + $1.credit.filter({$0.account == DEGAAPAccounts.income_deprAmort}).reduce(.zero, {$0 + $1.value}) })
			let recordsAppreciationValueGWG = recordsOnRetirementDay.reduce(.zero, {$0 + $1.credit.filter({$0.account == DEGAAPAccounts.income_deprAmort_gwg}).reduce(.zero, {$0 + $1.value}) })
			let retiredAssetsDepreciatedValue = retiredAssets.filter{($0.retirementDate == retirementDay) && !$0.isGWG}.reduce(.zero, {$0 + $1.depreciatedValue(for: retirementDay)})
			let retiredAssetsDepreciatedValueGWG = retiredAssets.filter{($0.retirementDate == retirementDay) && $0.isGWG}.reduce(.zero, {$0 + $1.depreciatedValue(for: retirementDay)})
			if (recordsAppreciationValue != retiredAssetsDepreciatedValue) || (recordsAppreciationValueGWG != retiredAssetsDepreciatedValueGWG)
			{
				issues.append(Issue(domain: .ledger, code: .retiredFixedAssetsNotAppreciatedInJournal, metadata: [.day : retirementDay.bh]))
			}
		}
	}

}
