# ``Buchhaltung``

Provides basic bookkeeping functions for very small, limited liability capital companies (GmbH, UG) in Germany.

## Overview

Accounting data for a business period is stored in ``Ledger`` instances.
Ledger is composed of
- ``Company``, which stores the legal information of the company
- ``Journal``, which stores ``Record`` instances that record booking operations
- ``Inventory``, which stores inventory items like
   - ``Inventory/FixedAsset``
   - ``Inventory/CurrentAsset``

Note that the majority of the code documentation is in German, using the legal German nomenclature
for business reports and referring to German trade laws.

## Topics

### Core Classes and Serialization

- ``Ledger``
- ``Report``

### Generating a business report

- ``Ledger/generateReport(type:title:withInventory:)``

### BH serialization

- ``Ledger/archive()``
- ``Ledger/init(fromArchive:issues:)``

- ``Report/archive()``
- ``Report/init(fromArchive:issues:)``

### Generating LaTeX files

- ``LaTeXEncoder/encode(_:)``

### Generating myebilanz files

- ``MyebilanzExporter/export(report:baseReport:)``
