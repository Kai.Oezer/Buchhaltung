// Copyright 2023-2024 Kai Oezer

import DEGAAP
import Buchhaltung
import ArgumentParser
import IssueCollection

struct AccountTrackingOptions : ParsableArguments
{
	@Argument(help: "The XBRL name of the account to track. Example: bs.ass.currAss.cashEquiv.bank")
	var account : AccountIDArgument

	@Argument(help: "The day up to, and including, which the account value must be tracked. The day must be given in ISO 8601 format. Example: 2023-06-21")
	var date : DayArgument
}

struct AccountTrackingSubcommand : AsyncParsableCommand
{
	static var _commandName : String { "account" }
	static var configuration : CommandConfiguration { CommandConfiguration(abstract: "Prints the total for the given account and given date.") }

	@OptionGroup var commonOptions : CommonOptions
	@OptionGroup var options : AccountTrackingOptions

	mutating func run() async throws
	{
		var issues = IssueCollection()
		let degaap = DEGAAPEnvironment()

		let ledger = try Utils.load(Ledger.self, from: commonOptions.ledgerPath, issues: &issues)
		if issues.count > 0 {
			print("Issues detected:\n\(issues)")
		}

		let total = await ledger.accountTotal(for: options.account.accountID, upTo: options.date.day, using: degaap)

		print("total of \(options.account) up to \(options.date): \(total)")
	}
}
