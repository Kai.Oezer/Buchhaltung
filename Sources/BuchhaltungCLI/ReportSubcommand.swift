// Copyright 2020-2024 Kai Oezer

import Foundation
import DEGAAP
import Buchhaltung
import ArgumentParser
import IssueCollection

struct ReportOptions : ParsableArguments
{
	@Option(name: .shortAndLong, help: "Where to place the generated documents. The default location is the working directory.")
	var output : FolderPath?

	@Option(name: .shortAndLong, help: "The title of the report. The default title is 'Jahresabschluss <YEAR>'. If the title contains space characters, it needs to be enclosed in quotes.")
	var title : String?

	@Option(name: [.customLong("previous-report"), .customShort("p")], help: "The path to a previously generated report file from which to extract the results of the previous business period.")
	var previous : FilePath?

	@Flag(name: .long, help: "Whether to generate a fiscal report instead of the default public report.")
	var fiscal = false

	@Flag(name: .long, help: "Whether to generate the inventory statement as part of the report.")
	var inventory = false

	@Flag(name: .long, help: "Whether to also generate a LaTeX document.")
	var latex = false

	@Flag(name: .long, help: "Whether to also generate myebilanz files (main INI file and account totals CSV file).")
	var myebilanz = false
}

struct ReportSubcommand : AsyncParsableCommand
{
	static var _commandName : String { "report" }
	static var configuration : CommandConfiguration { CommandConfiguration(abstract: "Generates a business report.") }

	@OptionGroup var commonOptions : CommonOptions
	@OptionGroup var options : ReportOptions

	mutating func run() async throws
	{
		var issues = IssueCollection()
		let degaap = DEGAAPEnvironment()

		let ledger : Ledger = try {
			var ledger_ = try Utils.load(Ledger.self, from: commonOptions.ledgerPath, issues: &issues)
			ledger_.baseReport = (options.previous != nil) ? try Utils.load(Report.self, from: options.previous!, issues: &issues) : nil
			if issues.count > 0 {
				print("Issues detected:\n\(issues)")
			}
			return ledger_
		}()

		let reportTitle = options.title ?? "Jahresabschluss \(ledger.period.year)"
		let reportType : ReportType = options.fiscal ? .fiscal : .trade
		let generateInventory = options.inventory
		let report = try await ledger.generateReport(type: reportType, title: reportTitle, withInventory: generateInventory, using: degaap, issues: &issues)

		let outputFolder = options.output ?? .workingDir
		let outputFolderLocation = outputFolder.url
		let outputFileBaseName = commonOptions.ledgerPath.fileBaseName
		try Utils.write(contents: report.archive(), to: outputFileBaseName + ".bhreport", in: outputFolderLocation, overwrite: true)

		if options.latex
		{
			let latexRepresentation = try LaTeXEncoder.encode(report)
			try Utils.write(contents: latexRepresentation, to: outputFileBaseName + ".tex", in: outputFolderLocation, overwrite: true)
		}

		if options.myebilanz
		{
			var (iniContents, csvContents) = try MyebilanzExporter.exportWithCSV(report: report)
			let baseName = report.title.string.replacingOccurrences(of: " ", with: "_")
			let iniFileName = baseName + ".ini"
			let csvFileName = baseName + "-Salden.csv"
			iniContents = iniContents.replacingOccurrences(of: "<csv file name>", with: csvFileName)
			try Utils.write(contents: iniContents, to: iniFileName, in: outputFolderLocation, overwrite: true)
			try Utils.write(contents: csvContents, to: csvFileName, in: outputFolderLocation, overwrite: true)
		}
	}
}
